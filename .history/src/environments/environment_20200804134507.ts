// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  E_BASE_API_URL: 'http://pawnapplication.dzyte.com/',
  // 'http://pawnapplication.dzyte.com/',  //server
   //'http://localhost:8082/pawn_ci_api/' ,   // local
   'http://localhost/pawn-service/',   // local
  PER_PAGE: 25
};

