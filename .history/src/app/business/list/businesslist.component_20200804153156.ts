import { Component, OnInit } from '@angular/core';
import {IBussinessList} from './businesslist';
import {Router, ActivatedRoute} from '@angular/router';
import {BusinessListService} from './businesslist.services';
import {GenSettingsService} from '../../settings/general/gensettings.services';
import { Globals } from '../../../global';

@Component({
  selector: 'app-user-profile',
  templateUrl: './businesslist.component.html',
  styleUrls: ['./businesslist.component.scss']
})

export class BusinessListComponent implements OnInit {
  user_id: string = '';
  showLoading: boolean = true;
  public userBusinessList: Array<IBussinessList>;
  serverUrl = Globals.CI_BASE_URL;
  public checkList : number = 0;

  constructor(private _busServices:BusinessListService,private _genSettings:GenSettingsService,private router: Router,private route:ActivatedRoute,) { 
    this.user_id = localStorage.getItem("user_id");
  }

  

  ngOnInit() {

    this.userBusinessList = [];
    var businessList = this.route.snapshot.data['businessList'];
    //window.console.log('@#$@#');
    console.log('buss ????',businessList);
    this.userBusinessList = businessList;
    this.checkList = Number(localStorage.getItem('user_varient'));
     
   //console.log('checkList', this.checkList);
    this.showLoading = false;

  }
  GoToDashboard(business: any) {
    this.showLoading = true;
    
   // this.showLoading = false;

    localStorage.setItem('bus_id', business.business_id);
    localStorage.setItem('bus_name', business.business_name);
    localStorage.setItem('bus_photo', business.Photo);
    this._genSettings.GetBusSettings(business.business_id)
      .subscribe(
        (data: any) => {
        console.log('data', data)
        const setting = data.find(element => element.bus_id === business.business_id);
        console.log('setting', setting);
        if (this.checkList === 2) {
          if (setting !== undefined) {
            this.router.navigate(['/dashboard/']);
          } else {
            this.router.navigate(['/settings/']);
          }
        }else if (this.checkList === 1) {
          this.router.navigate(['/dashboard/']);
        }
         

        },
        error => {
          window.console.log(error);
          alert('Error on Submition');
        }
      );
    // 
    // this.showLoading = false;

  }

  logout() {
    localStorage.removeItem('email');
    localStorage.removeItem('first_name');
    localStorage.removeItem('last_name');
    localStorage.removeItem('photo');
    localStorage.removeItem('user_id');
    localStorage.removeItem('bus_id');
    localStorage.removeItem('bus_name');
    localStorage.removeItem('bus_photo');
    localStorage.removeItem('user_varient');
    this.router.navigate(['/login']);
  }

  deleteBusiness(business) {
    //console.log('business', business)
    const formData = new FormData();
    const that = this;
    formData.append('bus_id', business.business_id);
    this._genSettings.deleteBusiness(formData).subscribe((deleteBus: any) => {
     // console.log('delete', deleteBus);


     this.router.navigateByUrl('dashboard', { skipLocationChange: true }).then(() =>
     that.router.navigate(['/']));

    //   this.router.routeReuseStrategy.shouldReuseRoute = () => true;
    // this.router.onSameUrlNavigation = 'reload';
    // this.router.navigate(['/']);
    }, error => {
      window.console.log(error);
      alert('Error on Submition');
    });
  }


}
