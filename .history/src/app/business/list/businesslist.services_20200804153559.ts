import { Injectable } from '@angular/core';
import { HttpClient ,HttpHeaders ,HttpParams, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map, filter, catchError, mergeMap } from 'rxjs/operators'
import {IBussinessList} from './businesslist';
import { Globals } from '../../../global';
import { RequestOptionsArgs, RequestMethod, RequestOptions } from '@angular/http';
@Injectable()

export class BusinessListService{


    constructor(private http: HttpClient){ 
    }     

    GetBusiness(user_id:string):Observable<IBussinessList[]>{  
        const headers = new HttpHeaders();        
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        return this.http.get<IBussinessList[]>(Globals.CI_BASE_URL+"GetUserBusiness/?userID="+user_id,{headers: headers});
    }

    refreshBusiness(){
        
    return this._businessService.GetBusiness(localStorage.getItem("user_id"));
    }

    

}