import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { RegisterComponent } from './register/user/register.component';
import { BusRegisterComponent } from './register/business/bregister.component';
import { BusinessListComponent } from './business/list/businesslist.component';
import { GenSettingsComponent } from './settings/general/gensettings.component';
import { BusinessResolve } from './business/list/businesslist.resolve';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './login/logout.component';
import { AuthGuard } from './guard/auth.guard';


const routes: Routes = [
  // {
  //   path: 'dashboard',
  //   redirectTo: 'dashboard',
  //   pathMatch: 'full',
  // },
  {
    path: 'register',
    component: RegisterComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard],
  },
  {
    path: 'login',
    component: LoginComponent,
    pathMatch: 'full',
  },
  {
    path: 'logout',
    component: LogoutComponent,
    pathMatch: 'full',
  },
  {
    path: 'business/:userid',
    component: BusRegisterComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard]
  },
  {
    path: 'business',
    component: BusinessListComponent,
    resolve: {
      businessList: BusinessResolve
    },
    pathMatch: 'full',
    // canActivate: [AuthGuard]
  },
  {
    path: '',
    component: BusinessListComponent,
    resolve: {
      businessList: BusinessResolve
    },
    pathMatch: 'full',
    // canActivate: [AuthGuard]
  },
  {
    path: 'settings',
    component: GenSettingsComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard]
  }
  , {
    path: '',
    component: AdminLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule'
      }],
    
  },

];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes, { useHash: true })
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
