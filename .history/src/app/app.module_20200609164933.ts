import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';


import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';
import { RegisterModule } from './register/user/register.module';
import { BusRegisterModule } from './register/business/bregister.module';
import { LoginModule } from './login/login.module';
import { BusinessResolve } from './business/list/businesslist.resolve';
import {BusinesslistModule} from './business/list/businesslist.module';
import {GenSettingsModule} from './settings/general/gensettings.module';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';

import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './login/logout.component';
import { BusRegisterComponent } from './register/business/bregister.component';
import { BusinessListComponent } from './business/list/businesslist.component';
import { GenSettingsComponent } from './settings/general/gensettings.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { AppBootstrapModule } from './app-bootstarp/app-bootstrap.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';




@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ComponentsModule,
    RouterModule,
    GenSettingsModule,
    AppRoutingModule,
    BusinesslistModule,
    NgbModule,
    RegisterModule,
    LoginModule,
    BusRegisterModule,
    AngularMultiSelectModule,
    ToastrModule.forRoot(),
    AppBootstrapModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    BusRegisterComponent,
    BusinessListComponent,
    LoginComponent,
    LogoutComponent,
    GenSettingsComponent,
  ],
  providers: [BusinessResolve],
  bootstrap: [AppComponent]
})
export class AppModule { }
