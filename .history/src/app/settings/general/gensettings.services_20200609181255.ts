import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map, filter, catchError, mergeMap } from 'rxjs/operators'
//import {IUserList} from './users';
import { Globals } from '../../../global';
import { RequestOptionsArgs, RequestMethod, RequestOptions } from '@angular/http';
@Injectable()

export class GenSettingsService {

    constructor(private http: HttpClient) {

    }

    AddSettings(value) {
        window.console.log(value);
        let headerOptions = new HttpHeaders();
        headerOptions.append('Access-Control-Allow-Credentials', 'true');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

        return this.http.post(Globals.CI_BASE_URL + "AddBusiness", value, {
            headers: headerOptions,
        });

    }

    AddBusSettings(value) {
        console.log('form data', value)
        let headerOptions = new HttpHeaders();
        headerOptions.append('Access-Control-Allow-Credentials', 'true');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        return this.http.post(Globals.CI_BASE_URL + "AddBusSettings", value, {
            headers: headerOptions,
        });
    }
    AddregisterData(value) {
        console.log('form data', value)
        let headerOptions = new HttpHeaders();
        headerOptions.append('Access-Control-Allow-Credentials', 'true');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        return this.http.post(Globals.CI_BASE_URL + "RegisterAdd", value, {
            headers: headerOptions,
        });
    }

    closedRegister(value) {
        console.log('form data', value)
        let headerOptions = new HttpHeaders();
        headerOptions.append('Access-Control-Allow-Credentials', 'true');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        return this.http.post(Globals.CI_BASE_URL + "RegisterClose", value, {
            headers: headerOptions,
        });
    }

    GetSettings() {
        let headerOptions = new HttpHeaders();
        headerOptions.append('Access-Control-Allow-Credentials', 'true');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        return this.http.get(Globals.CI_BASE_URL + "settingList", { headers: headerOptions });
    }

    GetItems() {
        let headerOptions = new HttpHeaders();
        headerOptions.append('Access-Control-Allow-Credentials', 'true');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        return this.http.get(Globals.CI_BASE_URL + "itemList/?rand="+ Math.random(), { headers: headerOptions });
    }

    GetBusSettings(bus_id: string) {
        let headerOptions = new HttpHeaders();
        headerOptions.append('Access-Control-Allow-Credentials', 'true');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        return this.http.get(Globals.CI_BASE_URL + "GetBusinessSettings/?bus_id=" + bus_id+'&rand='+ Math.random(), { headers: headerOptions });
    }

    deleteBusiness(bus_id) {
        const headerOptions = new HttpHeaders();
        headerOptions.append('Access-Control-Allow-Credentials', 'true');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        return this.http.post(Globals.CI_BASE_URL + 'deleteBusiness', bus_id, {
            headers: headerOptions,
        });
    }


}
