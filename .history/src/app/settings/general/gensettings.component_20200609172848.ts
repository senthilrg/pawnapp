import { Component, OnInit } from '@angular/core';
import { GenSettingsService } from './gensettings.services';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-user-profile',
  templateUrl: './gensettings.component.html',
  styleUrls: ['./gensettings.component.scss']
})
export class GenSettingsComponent implements OnInit {
  frmGensettings: FormGroup;
  dropdownSettings = {};
  LanguageList = [];
  areaSettings = {};
  areaList = [];
  goldItems = [];
  silverItems = [];

  SelectedLanguage = [];
  SelectedAreas = [];
  selectedLanguage = [];
  showLoading: boolean = true;
  showGenSettings: boolean = true;
  showBusSettings: boolean = false;

  genActive: string = 'active';
  busActive: string = '';
  golditems: FormArray;
  silveritems: FormArray;

  constructor(private router: Router, private route: ActivatedRoute, private _genSettingService: GenSettingsService, private formBuilder: FormBuilder) {
    this.showLoading = true;
    this.showGenSettings = true;
    this.showBusSettings = false;
    this.genActive = 'active';
    this.busActive = '';
  }

  ngOnInit() {
    this.goldItems = [];

    this.silverItems = [];

    this.frmGensettings = this.formBuilder.group({
      interest_rate: [''],
      selectedLanguage: [''],
      selectedArea: [''],
      item_name: [''],
      metal_catg: [''],
      bill_ref_1: [''],
      bill_ref_2: [''],
      gold_rate: [''],
      gold_rate_interest: this.formBuilder.array([this.goldRateofInterest]),
      silver_rate: [''],
      silver_rate_interest: this.formBuilder.array([this.silverRateofInterest]),
    });

    this.dropdownSettings = {
      singleSelection: true,
      text: 'Select',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: false,
      classes: 'myclass custom-class'
    };
    this.areaSettings = {
      singleSelection: false,
      text: 'Select',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: false,
      classes: 'myclass custom-class'
    };
    this.LanguageList = [
      { 'id': 'en', 'itemName': 'English' },
      { 'id': 'fr', 'itemName': 'French' }
    ];

    this.areaList = [
      { 'id': 'ar', 'itemName': 'Ariyankuppam' },
      { 'id': 'py1', 'itemName': 'Puducherry' },
      { 'id': 'or', 'itemName': 'Orleanpet' },
      { 'id': 'oz', 'itemName': 'Olgerate' }
    ];
    const busId = localStorage.getItem('bus_id');

    this._genSettingService.GetBusSettings(busId)
      .subscribe(
        (data: any) => {
          console.log('array setting value', data);
          if (data.length > 0) {
            var business_settings = data.filter(settings => settings.bus_id === busId);

            //console.log(business_settings);
            this.showGenSettings = false;
            this.showBusSettings = true;
            this.busActive = 'active';
            this.genActive = '';
            if (business_settings.length > 0) {

              var billno1 = business_settings.find(business_settings => business_settings.bus_settings_name === 'bill_ref_1');
              var billno2 = business_settings.find(business_settings => business_settings.bus_settings_name === 'bill_ref_2');

              if (billno1 != undefined)
                this.frmGensettings.get('bill_ref_1').setValue(billno1.bus_settings_value);

              if (billno2 != undefined)
                this.frmGensettings.get('bill_ref_2').setValue(billno2.bus_settings_value);

              var silver_rate = business_settings.find(business_settings => business_settings.bus_settings_name === 'silver_rate');
              const silver_interest = data.find(data => data.bus_settings_name === 'silver_interest');
              const silver_rate_interest = data.find(data => data.bus_settings_name === 'silver_rate_interest');

              if (silver_rate_interest != undefined) {
                const sil_obj = silver_rate_interest.bus_settings_value.replace(/"/g, '"');
                let sil_array = JSON.parse(sil_obj);
                sil_array = sil_array as FormArray;
                this.frmGensettings.get("silver_rate_interest").patchValue(sil_array);
              }

              if (silver_rate != undefined) {
                this.frmGensettings.get('silver_rate').setValue(silver_rate.bus_settings_value);
              }



              var gold_rate = business_settings.find(business_settings => business_settings.bus_settings_name === 'gold_rate');
              const gold_interest = data.find(data => data.bus_settings_name === 'gold_interest');
              const gold_rate_interest = data.find(data => data.bus_settings_name === 'gold_rate_interest');

              if (gold_rate_interest != undefined) {
                const gol_obj = gold_rate_interest.bus_settings_value.replace(/"/g, '"');
                let gol_array = JSON.parse(gol_obj);
               //const arr_data =  Object.assign({}, gol_array);
                gol_array = gol_array as FormArray;
                console.log('type check', typeof (gol_array));
                console.log('gol_array', gol_array);
                // while (gol_array.length) {
                //   gol_array.removeAt(0);

                // }
                this.frmGensettings.get("gold_rate_interest").patchValue(gol_array);
              }


              if (gold_rate != undefined) {
                this.frmGensettings.get('gold_rate').setValue(gold_rate.bus_settings_value);
              }

            }



            data.forEach(element => {

              if (element.bus_settings_name == 'selectedLanguage') {
                var settings = JSON.parse(element.bus_settings_value);
                if (settings[0] != undefined) {
                  this.selectedLanguage = [{ 'id': settings[0].id, 'itemName': settings[0].itemName }];
                  this.frmGensettings.get('selectedLanguage').setValue(this.selectedLanguage);
                }
              }
              if (element.bus_settings_name == 'selectedAreas') {
                var settings = JSON.parse(element.bus_settings_value);
                var bus_areas = {};
                var bus_areas_array = [];
                settings.forEach(element => {
                  if (element != undefined) {
                    bus_areas = { 'id': element.id, 'itemName': element.itemName };
                    bus_areas_array.push(bus_areas);
                  }
                });
                this.frmGensettings.get('selectedArea').setValue(bus_areas_array);
              }
              if (element.bus_settings_name == 'interest_rate') {
                this.frmGensettings.get('interest_rate').setValue(element.bus_settings_value);
              }
            });
          }

        },
        error => {
          window.console.log(error);
          alert('Error on Submition');
        }
      );
    this.GetSettingsItems();
  }

  get f() { return this.frmGensettings.controls; }

  get silverRateofInterest(): FormGroup {
    return this.formBuilder.group({
      chargeFromSilver: ['0'],
      chargeToSilver: ['0'],
      chargePercentSilver: ['0']
    });
  }
  get goldRateofInterest(): FormGroup {
    return this.formBuilder.group({
      chargeFromGold: ['0'],
      chargeToGold: ['0'],
      chargePercentGold: ['0']
    });
  }
  addsilverservice(): void {
    this.silveritems = this.frmGensettings.get('silver_rate_interest') as FormArray;
    this.silveritems.push(this.silverRateofInterest);
  }
  addgoldservice(): void {
    this.golditems = this.frmGensettings.get('gold_rate_interest') as FormArray;
    this.golditems.push(this.goldRateofInterest);
  }

  removegold(index): void {
    this.golditems.removeAt(index);

  }
  removesilver(index): void {
    this.silveritems.removeAt(index);

  }

  logout() {
    localStorage.removeItem('email');
    localStorage.removeItem('first_name');
    localStorage.removeItem('last_name');
    localStorage.removeItem('photo');
    localStorage.removeItem('user_id');
    localStorage.removeItem('bus_id');
    localStorage.removeItem('bus_name');
    localStorage.removeItem('bus_photo');
    localStorage.removeItem('user_name');
    this.router.navigate(['/login']);
  }

  addItem() {

    //window.console.log(this.goldItems.some(e => e.value === this.frmGensettings.get('item_name').value));
    if (this.frmGensettings.get('metal_catg').value == 'silver') {
      var itm_val: string = this.frmGensettings.get('item_name').value;
      //alert(this.camalize(itm_val));
      if (!this.silverItems.some(e => e.value === this.camalize(itm_val))) {
        let itmObj = { value: this.camalize(itm_val), type: this.frmGensettings.get('metal_catg').value };
        this.silverItems.push(itmObj);
      } else {
        alert('Items already exists');
      }
    }
    if (this.frmGensettings.get('metal_catg').value == 'gold') {
      var gitm_val = this.frmGensettings.get('item_name').value;

      if (!this.goldItems.some(e => e.value === this.camalize(gitm_val))) {
        let itmObj = { value: this.camalize(gitm_val), type: this.frmGensettings.get('metal_catg').value };
        this.goldItems.push(itmObj);
      } else {
        alert('Items already exists');
      }
    }
    this.frmGensettings.get('item_name').setValue('');
  }

  ClickSettingsTab(tab: string) {
    if (tab == 'gen') {
      this.showGenSettings = true;
      this.showBusSettings = false;
      this.genActive = 'active';
      this.busActive = '';
    } else if (tab == 'bus') {
      this.showGenSettings = false;
      this.showBusSettings = true;
      this.busActive = 'active';
      this.genActive = '';
    }
  }

  camalize(str: string) {
    /*return  (' ' + str).toLowerCase().replace(/[^a-zA-Z0-9]+(.)/g, function(match, chr)
    {
        return chr.toUpperCase();
    });*/
    return str.toUpperCase();
  }

  onLangItemSelect(item: any) {
    //console.log(item);
    //console.log(this.frmGensettings.get('selectedLanguage').value);
    this.SelectedLanguage = [];
    this.SelectedLanguage = this.frmGensettings.get('selectedLanguage').value;
  }
  OnLangItemDeSelect(item: any) {
    //console.log(item);
    //console.log(this.frmGensettings.get('selectedLanguage').value);
    this.SelectedLanguage = [];
    this.SelectedLanguage = this.frmGensettings.get('selectedLanguage').value;
  }
  onLangSelectAll(items: any) {
    //console.log(items);
    this.SelectedLanguage = [];
    this.SelectedLanguage = items
  }
  onLangDeSelectAll(items: any) {
    //console.log(items);
    this.SelectedLanguage = [];
    //this.SelectedLanguage.push(items);
  }

  onAreaItemSelect(item: any) {
    //console.log(item);
    //console.log(this.frmGensettings.get('selectedArea').value);
    this.SelectedAreas = [];
    this.SelectedAreas = this.frmGensettings.get('selectedArea').value;
  }
  OnAreaItemDeSelect(item: any) {
    //console.log(item);
    //console.log(this.frmGensettings.get('selectedArea').value);
    this.SelectedAreas = [];
    this.SelectedAreas = this.frmGensettings.get('selectedArea').value;
  }
  onAreaSelectAll(items: any) {
    //console.log(items);
    this.SelectedAreas = [];
    this.SelectedAreas = items;
  }
  onAreaDeSelectAll(items: any) {
    // console.log(items);
    this.SelectedAreas = [];
  }

  onSubmit() {
    console.log('form submit', this.frmGensettings.value)
    console.log(this.goldItems, '??');
    console.log(this.silverItems, '000');
    // return false

    // stop here if form is invalid
    if (this.frmGensettings.invalid) {
      return;
    }
    this.showLoading = true;


    const formData = new FormData();
    formData.append('selectedAreas', JSON.stringify(this.frmGensettings.value.selectedArea));
    formData.append('selectedLanguage', JSON.stringify(this.frmGensettings.value.selectedLanguage));


    formData.append('gold_items', JSON.stringify(this.goldItems));
    formData.append('silver_items', JSON.stringify(this.silverItems));
    formData.append('user_name', localStorage.getItem('user_name'));

    this._genSettingService.AddSettings(formData)
      .subscribe(
        (data: any) => {
          this.showLoading = false;
        },
        error => {
          alert('Error on Submition');
          this.showLoading = false;
        }
      );

    /** Adding Business Settings */
    const busSettingsData = new FormData();
    busSettingsData.append('bill_ref_1', this.frmGensettings.value.bill_ref_1);
    busSettingsData.append('bill_ref_2', this.frmGensettings.value.bill_ref_2);
    busSettingsData.append('gold_rate', this.frmGensettings.value.gold_rate);
    busSettingsData.append('gold_rate_interest', JSON.stringify(this.frmGensettings.value.gold_rate_interest));
    busSettingsData.append('silver_rate', this.frmGensettings.value.silver_rate);
    busSettingsData.append('silver_rate_interest', JSON.stringify(this.frmGensettings.value.silver_rate_interest));
    busSettingsData.append('bus_id', localStorage.getItem('bus_id'));
    busSettingsData.append('user_name', localStorage.getItem('user_name'));

    this._genSettingService.AddBusSettings(busSettingsData)
      .subscribe(
        (data: any) => {
          window.console.log(data);    
          this.showLoading = false;
          this.router.navigate(['/dashboard/']);
        },
        error => {
         // alert('Error on Submition');
          this.showLoading = false;
        }
      );
     // 
  }
  remove(type: string, value: string) {
    if (type == 'gold') {
      window.console.log(this.goldItems.find(x => x.value == this.camalize(value)));
      if (this.goldItems.find(x => x.value == this.camalize(value))) {
        this.goldItems.splice(this.goldItems.findIndex(x => x.value == this.camalize(value)), 1);
      }
    } else if (type == 'silver') {
      if (this.silverItems.find(x => x.value == this.camalize(value))) {
        this.silverItems.splice(this.silverItems.findIndex(x => x.value == this.camalize(value)), 1);
      }
    }
    window.console.log(this.goldItems);
    //window.console.log(this.silverItems);
  }

  GetSettingsItems() {
    this._genSettingService.GetItems()
      .subscribe(
        (data: any) => {
          // window.console.log(data);
          data.forEach(element => {
            if (element.item_type == 'gold') {
              let itmObj = { value: this.camalize(element.item_name), type: element.item_type };
              this.goldItems.push(itmObj);
            } else {
              let itmObj = { value: this.camalize(element.item_name), type: element.item_type };
              this.silverItems.push(itmObj);
            }

          });
          this.showLoading = false;
        },
        error => {
          this.showLoading = false;
          window.console.log(error);
          alert('Error on Submition');
        }
      );
  }

  dashBoard() {
    this.router.navigate(['dashboard']);
  }
}
