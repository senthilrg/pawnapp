import { Component, OnInit} from '@angular/core';
import { SwUpdate } from '@angular/service-worker';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private swUpdate: SwUpdate) {

  }
  ngOnInit() {
    this.swUpdate
    .available.subscribe(() => {
      console.log('[App] checkForUpdate completed');
      this.activateUpdate();
    });
  }

  activateUpdate() {
    const that = this;
    console.log('[App] activateUpdate started');
    this.swUpdate
      .activateUpdate()
      .then( () => {
        console.log('[App] activateUpdate completed');
      })
      .catch((err) => {
        console.error(err);
      });
  }

}
