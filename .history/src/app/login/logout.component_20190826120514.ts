import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-user-profile',
})

export class LogoutComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {

    // console.log('logout ?');
    localStorage.removeItem('email');
    localStorage.removeItem('first_name');
    localStorage.removeItem('last_name');
    localStorage.removeItem('photo');
    localStorage.removeItem('user_id');
    localStorage.removeItem('bus_id');
    localStorage.removeItem('bus_name');
    localStorage.removeItem('bus_photo');
    this.router.navigate(['/']);
  }

}
