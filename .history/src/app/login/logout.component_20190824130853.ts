import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.services';
import {Router,ActivatedRoute} from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-profile',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LogoutComponent implements OnInit {
  loginForm: FormGroup;  
  submitted = false;
  ShowUsrForm:Boolean = true;
  showError:Boolean = false;
  errorMsg:string = "";
  user_id:string = "";

  constructor(private router:Router,private route:ActivatedRoute,private _loginService:LoginService,private formBuilder: FormBuilder) {
    this.showError = false;
    this.errorMsg = "";
    if(localStorage.getItem("user_id")!=""){
      this.router.navigate(['/businesslist/'])
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
        user_id: ['', Validators.required],
        pwd: ['', Validators.required]
    });

    localStorage.setItem("email","");
    localStorage.setItem("first_name","");
    localStorage.setItem("last_name","");
    localStorage.setItem("photo","");
    localStorage.setItem("user_id","");
    localStorage.setItem("bus_id","");
    localStorage.setItem("bus_name","");
    localStorage.setItem("bus_photo","");
    this.router.navigate(['/']);
  }  

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
      this.submitted = true;
      //window.console.log(this.loginForm);
      
      // stop here if form is invalid
      if (this.loginForm.invalid) {
          return;
      }

      const formData = new FormData();
      formData.append("user_id", this.loginForm.get("user_id").value);
      formData.append("pwd", this.loginForm.get("pwd").value);
      
       this.showError = false;
       this._loginService.LoginCheck(formData)
       .subscribe(
         (data:any)=>{      
           
          // window.console.log(data);
           if(data.result=="NOK" && data.error==1){
             this.showError = true;
             this.errorMsg = data.msg;
           }else{
             this.showError = false;
             window.console.log(data.user_details);
             if(data.user_details!=""){
              localStorage.setItem("email",data.user_details.email);
              localStorage.setItem("first_name",data.user_details.first_name);
              localStorage.setItem("last_name",data.user_details.last_name);
              localStorage.setItem("photo",data.user_details.photo);
              localStorage.setItem("user_id",data.user_details.user_id);
             }
             
             // route to business
            this.router.navigate(['/businesslist/'])
           }
           //this.UserList = data;
         },
         error=>{
           alert("Error on Submition");
           
         }
       );
       
  }

}
