import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map, filter, catchError, mergeMap } from 'rxjs/operators';
// import {IUserList} from './users';
import { Globals } from '../../global';
import { RequestOptionsArgs, RequestMethod, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable()

export class LoginService {

    constructor(private http: HttpClient, private router: Router) {
    }

    LoginCheck(value) {
        window.console.log(value);
        const headerOptions = new HttpHeaders();
        headerOptions.append('Access-Control-Allow-Credentials', 'true');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

        const httpParams = new HttpParams().set('values', JSON.stringify(value));

        return this.http.post(Globals.CI_BASE_URL + 'login', value, {
            headers: headerOptions,
        }).toPromise().then((data: any) => {
            if (data.result === 'OK' && data.error === 0) {
                if (data.user_details !== '') {
                    localStorage.setItem('email', data.user_details.email);
                    localStorage.setItem('first_name', data.user_details.first_name);
                    localStorage.setItem('last_name', data.user_details.last_name);
                    localStorage.setItem('photo', data.user_details.photo);
                    localStorage.setItem('user_id', data.user_details.user_id);
                    localStorage.setItem('user_name', data.user_details.user_name);
                    localStorage.setItem('user_varient', data.user_details.user_type);
                }

                // route to business
                this.router.navigate(['/']);
            }
        });

    }
    GetBusSettings(bus_id: string) {
        let headerOptions = new HttpHeaders();
        headerOptions.append('Access-Control-Allow-Credentials', 'true');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        return this.http.get(Globals.CI_BASE_URL + "GetBusinessSettings/?bus_id=" + bus_id + '&rand=' + Math.random(), { headers: headerOptions });
    }


    GetRegister(bus_id) {
        let headerOptions = new HttpHeaders();
        headerOptions.append('Access-Control-Allow-Credentials', 'true');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

        return this.http.get(Globals.CI_BASE_URL + "RegisterList/?bus_id=" + bus_id + '&rand=' + Math.random(), {
            headers: headerOptions,
        });
    }

    get isLoggedIn(): boolean {
        const user = JSON.parse(localStorage.getItem('user_id'));
        console.log('user', user)
        return user !== null;
    }
    get isBusinessIn(): boolean {
        const bus_id = localStorage.getItem('bus_id');
        console.log('bus_id', bus_id)
        return bus_id !== null;
    }

    async isBusinessSettingIn() {
        const bus_id = localStorage.getItem('bus_id');
        let setting = undefined;
        let finalsetting = undefined;
        await this.GetBusSettings(bus_id).toPromise().then(async (data : any) => {
            setting = await  data.find(element => element.bus_id === bus_id);
        })
        
           
            finalsetting = await setting;
            console.log('finalsetting', finalsetting);
        if (finalsetting !== undefined) {
            return true;
        } else {
            return false
        }

    }
    async isRegisterIn() {
        const bus_id = localStorage.getItem('bus_id');
        let setting = undefined;
       await this.GetRegister(bus_id).toPromise().then(async (data : any) => {
           console.log('register', data);
            if (data.length) {
                const date1: any = new Date(data[0].created_date);
                const date2: any = new Date();
                const diffTime = Math.abs(date2 - date1);
                const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                if (diffDays > 1) {
                    const obj = {
                        status : 'Please close the register',
                        condition : false
                    };

                    setting = await obj
                } else {
                    const obj = {
                        status : '',
                        condition : true
                    };
                    setting = await obj
                }
             } else {
                const obj = {
                    status : 'Please Add Cash Register',
                    condition : false
                };
                setting = await obj
             }
            
        });
        console.log('setting outside', setting);  
        return await  setting;
       
       

    }



}
