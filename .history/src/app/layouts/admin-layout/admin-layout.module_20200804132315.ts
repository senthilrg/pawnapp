import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule,DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { ChartsModule } from 'ng2-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { PledgeComponent } from '../../pledge/pledge.component';
import { EditPledgeComponent } from '../../pledge/manage/editpledge.component';
import { PledgelistComponent } from '../../pledge/list/pledgelist.component';
import { RedumptionComponent} from '../../redumption/redumption.component'
import { PledgeModule } from '../../pledge/pledge.module';
import { PledgelistModule } from '../../pledge/list/pledgelist.module';
import { DashboardModule } from '../../dashboard/dashboard.module';
import { EditPledgeModule } from '../../pledge/manage/editpledge.module';
import { RedeemComponent } from '../../redeem/redeem.component';
import { RegisterComponent } from '../../register/user/register.component';

import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    PledgeModule,
    PledgelistModule,
    EditPledgeModule,
    DashboardModule,
    ReactiveFormsModule,
    ChartsModule,
    NgbModule,
    ModalModule.forRoot(),
    AngularMultiSelectModule,
    ToastrModule.forRoot(),
    Ng4LoadingSpinnerModule.forRoot(),
  ],
  declarations: [
    DashboardComponent,
    PledgeComponent,
    EditPledgeComponent,
    PledgelistComponent,
    UserProfileComponent,
    TableListComponent,
    UpgradeComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    RedumptionComponent,
    RedeemComponent,
    RegisterComponent
  ],
  providers: [DatePipe],
})

export class AdminLayoutModule {}
