import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { PledgeComponent } from '../../pledge/pledge.component';
import { EditPledgeComponent } from '../../pledge/manage/editpledge.component';
import { PledgelistComponent } from '../../pledge/list/pledgelist.component';
import { RedumptionComponent } from '../../redumption/redumption.component';
import {RedeemComponent} from '../../redeem/redeem.component';
import { RegisterComponent } from '../../register/user/register.component';
import {} from '../../reports/reports.module';
import {BusinessGuard} from '../../guard/business.guard';


export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard', component: DashboardComponent  },
    { path: 'pledge/manage/:bill', component: EditPledgeComponent },
    { path: 'pledge', component: PledgeComponent , }, // canActivate: [BusinessGuard]
    { path: 'pledgelist/:bill', component: PledgelistComponent },
    { path: 'user-profile', component: UserProfileComponent },
    { path: 'table-list', component: TableListComponent },
    { path: 'typography', component: TypographyComponent },
    { path: 'icons', component: IconsComponent },
    { path: 'maps', component: MapsComponent },
    { path: 'notifications', component: NotificationsComponent },
    { path: 'redumption', component: RedumptionComponent },
    { path: 'upgrade', component: UpgradeComponent },
    { path: 'redeem',component: RedeemComponent},
    { path: 'expense', loadChildren: '../../expenses/expenses.module#ExpensesModule'},
    { path: 'reports', loadChildren: '../../reports/reports.module#ReportsModule'},
    { path: 'register', component: RegisterComponent},
];
