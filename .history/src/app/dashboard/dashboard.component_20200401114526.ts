import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DashboardService } from './dashboard.services';
import { GenSettingsService } from '../settings/general/gensettings.services';
import { PledgelistService } from '../pledge/list/pledgelist.services';
import * as Chartist from 'chartist';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  showLoading: boolean = true;
  bus_id: string = "";
  bill: string = "";
  todayDate: string = "";
  todayGoldrate: number = 0;
  todaySilverrate: number = 0;
  public close_balance = 0;
  public todayCashInHand: number = 0;
  public register_id: number = 0;
  public gold_rate_id = 0;
  public silver_rate_id = 0;
  public cih_status = false;
  public close_status = false;

  constructor(private router: Router, private route: ActivatedRoute, private _genSettingService: GenSettingsService,
    private datePipe: DatePipe, private _pledgeService: PledgelistService, private _dashboardService: DashboardService) {

   
  }

  ngOnInit() {
    if (localStorage.getItem("bus_id") != "" && localStorage.getItem("bus_id") != null) {
      this.bus_id = localStorage.getItem("bus_id");
    }/*else if(localStorage.getItem("user_id")=="" || this.bus_id==""){
      this.router.navigate(['/']);
    }*/
    if (localStorage.getItem("bus_id") == "") {
      this.router.navigate(['/']);
      return;
    }

    this._dashboardService.GetBusSettings(localStorage.getItem("bus_id"))
      .subscribe(
        (data: any) => {
          console.log('bus id', data);
          const silver_rate = data.find(data => data.bus_settings_name === 'silver_rate');
          const gold_rate = data.find(data => data.bus_settings_name === 'gold_rate');
          if (silver_rate != undefined) {
            this.silver_rate_id = silver_rate.bus_settings_id;
            this.todaySilverrate = Number(silver_rate.bus_settings_value);
          }
          if (gold_rate != undefined) {
            this.gold_rate_id = gold_rate.bus_settings_id;
            this.todayGoldrate = Number(gold_rate.bus_settings_value);
          }
        },
        error => {
          window.console.log(error);
          alert("Error on Submition");
        }
      );
    this.showLoading = false;
    this._dashboardService.GetRegister(localStorage.getItem("bus_id")).subscribe(
      (data: any) => {
        this.close_status = true;
        if (data.length) {
          window.console.log(data);
          this.register_id = Number(data[0].register_id);
          this.todayCashInHand = Number(data[0].starting_cash);
          const date1 : any = new Date(data[0].created_date);
          this.todayDate = this.datePipe.transform(date1, "dd-MM-yyyy");
          const date2 : any = new Date();
          const diffTime = Math.abs(date2 - date1);
          const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
          const cash_det = Number(data[0].cash_in) - Number(data[0].cash_out);
          this.close_balance = cash_det;
          this.cih_status = true;
          if (diffDays > 1) {
            alert('Please close the register');    
            return false;
          }else {
            this.close_status = false;
          }
         
        }

      },
      error => {
        window.console.log(error);
        alert("Error on Submition");
      }
    );
  }


  pledgePage() {
    this.router.navigate(['/pledge']);
  }

  closingregister() {
    // close_balance
    console.log('close register', this.close_balance);
    if (this.close_balance >= 0) {

      const closeData = new FormData();
      closeData.append('register_id', String(this.register_id));
      closeData.append('is_closed', '1');
      closeData.append('user_name', localStorage.getItem('user_name'));
      this._genSettingService.closedRegister(closeData).subscribe(
        (data: any) => {
          window.console.log(data);
          console.log('bus id', data);
          this.todayCashInHand = 0;
          this.close_balance = 0;
          this.register_id = 0;
          this.cih_status = false;
          this.close_status = true;
        },
        error => {
          window.console.log(error);
          alert("Error on Submition");
        }
      );

    }
  }

  cashregister() {
    this.cih_status = true;
    this.close_status = false;
    this.router.navigateByUrl('business', { skipLocationChange: true }).then(() =>
    this.router.navigate(['dashboard']));
    this.setRate();
  }

  setRate() {
    if ((this.todayGoldrate > 0) && (this.todaySilverrate > 0) && (this.todayCashInHand > 0))
      this.showLoading = true;
    const busSettingsData = new FormData();
    const gold_silver_summary = [
      {
        id: this.gold_rate_id,
        value: this.todayGoldrate,
        name: 'gold_rate'
      },
      {
        id: this.silver_rate_id,
        value: this.todaySilverrate,
        name: 'silver_rate'
      }
    ]
    busSettingsData.append('gold_silver_array', JSON.stringify(gold_silver_summary));
    busSettingsData.append('starting_cash', String(this.todayCashInHand));
    busSettingsData.append('bus_id', localStorage.getItem('bus_id'));
    busSettingsData.append('register_id', String(this.register_id));
    busSettingsData.append('cash_in', String(this.todayCashInHand));
    busSettingsData.append('cash_out', '0');
    busSettingsData.append('total_pledge', '0');
    busSettingsData.append('total_expense', '0');
    busSettingsData.append('total_interest', '0');
    busSettingsData.append('total_partial', '0');
    busSettingsData.append('is_closed', '0');
    busSettingsData.append('closed_date', '0');
    busSettingsData.append('user_name', localStorage.getItem('user_name'));
    console.log('busSettingsData', busSettingsData);
    if (this.register_id > 0) {
      busSettingsData.append('register_status', '1');
    } else {
      busSettingsData.append('register_status', '0');
    }
    this._genSettingService.AddregisterData(busSettingsData)
      .subscribe(
        (data: any) => {
          this.close_balance = this.todayCashInHand;
          console.log(data);
          this.showLoading = false;
         
        },
        error => {
          alert('Error on Submition');
          this.showLoading = false;
        }
      );
  }

  SearchPledge() {

    this._pledgeService.GetPledges(localStorage.getItem("bus_id"), this.bill)
      .subscribe(
        (data: any) => {

          if (data.length > 0) {
            this.router.navigate(['/pledgelist/' + this.bill]);
          }
        },
        error => {
          window.console.log(error);
          alert("Error on Submition");
        }
      );
  }
}
