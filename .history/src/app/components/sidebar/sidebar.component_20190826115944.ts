import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../global';

declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  { path: '/dashboard', title: 'Dashboard', icon: 'design_app', class: '' },
  { path: '/pledge', title: 'Pledge', icon: 'education_atom', class: '' },

  { path: '/register', title: 'User Register', icon: 'users_single-02', class: '' },
  { path: '/business', title: 'Business', icon: 'education_atom', class: '' },
  { path: '/businesslist', title: 'Manage Business', icon: 'ui-1_bell-53', class: '' },
  { path: '/settings', title: 'Settings', icon: 'objects_spaceship', class: '' },


  { path: '/redumption', title: 'Redumption', icon: 'objects_spaceship', class: '' },
  { path: '/repledge', title: 'Re-Pledge', icon: 'objects_spaceship', class: '' },
  /*{ path: '/icons', title: 'Icons',  icon:'education_atom', class: '' },
  { path: '/maps', title: 'Maps',  icon:'location_map-big', class: '' },
  { path: '/notifications', title: 'Notifications',  icon:'ui-1_bell-53', class: '' },

  { path: '/user-profile', title: 'User Profile',  icon:'users_single-02', class: '' },
  { path: '/table-list', title: 'Table List',  icon:'design_bullet-list-67', class: '' },
  { path: '/typography', title: 'Typography',  icon:'text_caps-small', class: '' },
  { path: '/upgrade', title: 'Upgrade to PRO',  icon:'objects_spaceship', class: 'active active-pro' }*/

];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  businessName: string = "";
  bus_photo: string = "";
  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.businessName = localStorage.getItem("bus_name");
    this.bus_photo = Globals.BUS_IMAGE + localStorage.getItem("bus_photo");
  }
  isMobileMenu() {
    if (window.innerWidth > 991) {
      return false;
    }
    return true;
  };
}
