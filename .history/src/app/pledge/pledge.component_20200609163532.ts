import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PledgeService } from './pledge.services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Globals } from '../../global';
import { distinctUntilChanged } from 'rxjs/operators';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-user-profile',
  templateUrl: './pledge.component.html',
  styleUrls: ['./pledge.component.scss']
})
export class PledgeComponent implements OnInit {
  frmpledge: FormGroup;
  submitted: Boolean = false;
  customerPath: string;
  selectedFile: File;
  uploadedFile: string = '';
  custFieldEnabled: Boolean = true;
  PledgeImgPath: string = '';
  dropdownSettings = {};
  ItemList = [];
  gold_rate_interest = [];
  silver_rate_interest = [];
  selectedPledgeItm: string = '';
  MainPledgeItems = [];
  PledgeImgName: string = '';
  showLoading: boolean = true;
  tot_pcs: number = 0;
  tot_wgt: number = 0;
  tot_wst: number = 0;
  tot_net_wgt: number = 0;
  gold_rate: number = 0;
  silver_rate: number = 0;
  public close_balance = 0;
  public checkList : number = 0;
  @ViewChild('infoModal') infoModal: ModalDirective;

  constructor(private router: Router, private route: ActivatedRoute, 
    private formBuilder: FormBuilder, private _pledgeService: PledgeService,
    private spinnerService: Ng4LoadingSpinnerService) {
    this.customerPath = '../../../assets/img/add-user.png';
    this.PledgeImgPath = '../../assets/img/add-prod.jpg';
    this.showLoading = true;
    this.checkList = Number(localStorage.getItem('user_varient'));
  }

  ngOnInit() {
    this.GetItemMaster();
    this.GetBusSettings();
    this.GetCashRegister();
    /*$('.item_img_holder img').on('click',function(){
      $(this).next('.clsImgProd').click();
    });*/


    this.frmpledge = this.formBuilder.group({
      bill_no_1: ['', Validators.required],
      bill_no_2: ['', Validators.required],
      date_of_bill: ['', Validators.required],
      cust_name: ['', Validators.required],
      father_name: ['', Validators.required],
      phone_no: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      area: ['', Validators.required],
      pin_code: ['', Validators.required],
      address_1: [''],
      address_2: [''],
      pledge_status: [1], // 1 - active , 2 - inactive,
      address_3: [''],
      price: [''],
      roi: [''],
      service_charge: [0],
      interest: [''],
      amount: ['', Validators.required],
      customerAvatar: [''],
      pledge_id: [0],
      pledgeItemAvatar: [''],
      find_user_input: [''],
      customer_id: [0],
      item_name: [''],
      no_of_pcs: [''],
      wgt_gms: [''],
      waste_gms: [''],
      net_wgt: [''],
      selectedPledgeItem: [''],
      selectedPledgeName: [''],
      selectedPledgeType: [''],
      gold_price: [0],
      gold_amount: [''],
      silver_amount: [''],
      gold_rate_interest: [''],
      gold_interest: [''],
      gold_charge_percent: [0],
      silver_price: [0],
      silver_rate_interest: [''],
      silver_interest: [''],
      silver_charge_percent: [0],
      goldsilvertotalamt: [],
      regular_gold_percent: [0],
      regular_silver_percent: [0],
      regular_gold_interest: [0],
      regular_silver_interest : [0],
      regular_gold_silver_amt : [0],
      regular_amount: [0]
    });

    this.dropdownSettings = {
      singleSelection: true,
      text: 'Select',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: false,
      classes: 'myclass custom-class'
    };
    this.frmpledge.get('date_of_bill').setValue(this.formatDate());
    this.showLoading = false;
    this.formControlValueChanged();
  }

  get f() { return this.frmpledge.controls; }

  formControlValueChanged() {

    this.frmpledge.get('service_charge').valueChanges.pipe(distinctUntilChanged()).subscribe(
      (mode: number) => {
        console.log('mode', mode)
        this.commoncalculation();
        this.regularcommoncalculation();
      });



    // const  this.usersForm.get('')
  }

  GetCashRegister() {
    this._pledgeService.GetRegister(localStorage.getItem('bus_id')).subscribe(
      (data: any) => {
       
        if (data.length) {
          window.console.log(data);
         
          const date1 : any = new Date(data[0].created_date);
          const date2 : any = new Date();
          const diffTime = Math.abs(date2 - date1);
          const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
          const cash_det = Number(data[0].cash_in) - Number(data[0].cash_out);
          this.close_balance = cash_det;
      
         
         
        }

      },
      error => {
        window.console.log(error);
        alert('Error on Submition');
      }
    );
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
    this.uploadedFile = this.selectedFile.name;

    var reader = new FileReader();

    this.frmpledge.get('customerAvatar').setValue(this.selectedFile);
    reader.onload = (event: any) => {
      //window.console.log(event.target.result);
      //window.console.log(event);
      this.customerPath = event.target.result;
    }
    reader.readAsDataURL(event.target.files[0])
  }

  onPledgeChanged(event) {
    this.frmpledge.get('pledgeItemAvatar').setValue(event.target.files[0]);
    // window.console.log(event.target.files[0].name);
    this.PledgeImgName = event.target.files[0].name;
    var reader = new FileReader();

    reader.onload = (event: any) => {
      this.PledgeImgPath = event.target.result;
    }
    reader.readAsDataURL(event.target.files[0])
  }

  onPledgeItemSelect(item: any) {
    this.selectedPledgeItm = item.id;
    this.frmpledge.get('selectedPledgeName').setValue(item.itemName);
    this.frmpledge.get('selectedPledgeType').setValue(item.itemType);
  }
  OnPledgeItemDeSelect(item: any) {
    this.selectedPledgeItm = '';
  }

  AddPledgeItem() {
    //alert('ASDF');
    const pledgeId = this.frmpledge.get('selectedPledgeItem').value;
const netWgt = (Number(this.frmpledge.get('no_of_pcs').value) *  Number(this.frmpledge.get('wgt_gms').value)) *
(Number(this.frmpledge.get('waste_gms').value) / 100);
console.log('netWgt', netWgt);
    let itm = {
      'imgPath': this.PledgeImgPath,
      'imgFile': this.PledgeImgName,
      'pledgeItm': (pledgeId) ? pledgeId[0]['id'] : 0,
      'itemName': this.frmpledge.get('selectedPledgeName').value,
      'itemType': this.frmpledge.get('selectedPledgeType').value,
      'no_of_pcs': this.frmpledge.get('no_of_pcs').value,
      'wgt_gms': this.frmpledge.get('wgt_gms').value,
      'waste_gms': this.frmpledge.get('waste_gms').value,
      'net_wgt': String(netWgt),
      'is_deleted' : 0,
      'pledge_item_id': 0
    }
    this.MainPledgeItems.push(itm);
    //window.console.log(this.MainPledgeItems);
    this.infoModal.hide();

    this.reCalcPledgeItems();
  }

  removePledgeItm(index) {
    this.MainPledgeItems.splice(index, 1);
    this.reCalcPledgeItems();
  }

  reCalcPledgeItems() {
    this.tot_pcs = 0;
    this.tot_net_wgt = 0;
    this.tot_wgt = 0;
    this.tot_wst = 0;
    const silverObj = {
      sil_pcs: 0,
      sil_wgt: 0,
      sil_wst: 0,
      sil_net_wgt: 0
    };
    const goldObj = {
      gol_pcs: 0,
      gol_wgt: 0,
      gol_wst: 0,
      gol_net_wgt: 0
    };

    if (this.MainPledgeItems.length > 0) {

      this.MainPledgeItems.forEach(element => {
        if (element.itemType == 'silver') {

          silverObj.sil_net_wgt = silverObj.sil_net_wgt + parseInt(element.net_wgt);
          silverObj.sil_pcs = silverObj.sil_pcs + parseInt(element.no_of_pcs);
          silverObj.sil_wgt = silverObj.sil_wgt + parseInt(element.wgt_gms);
          silverObj.sil_wst = silverObj.sil_wst + parseInt(element.waste_gms);
        } else if (element.itemType == 'gold') {
          goldObj.gol_net_wgt = goldObj.gol_net_wgt + parseInt(element.net_wgt);
          goldObj.gol_pcs = goldObj.gol_pcs + parseInt(element.no_of_pcs);
          goldObj.gol_wgt = goldObj.gol_wgt + parseInt(element.wgt_gms);
          goldObj.gol_wst = goldObj.gol_wst + parseInt(element.waste_gms);
        }
        this.tot_pcs = this.tot_pcs + parseInt(element.no_of_pcs);
        this.tot_wgt = this.tot_wgt + parseInt(element.wgt_gms);
        this.tot_wst = this.tot_wst + parseInt(element.waste_gms);
        this.tot_net_wgt = this.tot_net_wgt + parseInt(element.net_wgt);
      });


      this.frmpledge.get('gold_price').setValue(goldObj.gol_net_wgt * this.gold_rate);
      if (this.frmpledge.get('gold_price').value !== 0) {
        const goldPrice = goldObj.gol_net_wgt * this.gold_rate;
        this.frmpledge.get('gold_charge_percent').setValue(0);
        this.frmpledge.get('regular_gold_percent').setValue(1);
        
        for (const charge of this.gold_rate_interest) {

          if ((goldPrice >= charge.chargeFromGold) && (charge.chargeToGold >= goldPrice)) {

            this.frmpledge.get('gold_charge_percent').setValue(charge.chargePercentGold);
            // this.frmpledge.get('regular_gold_percent').setValue(1);

          }
        }

      }

      this.frmpledge.get('silver_price').setValue(silverObj.sil_net_wgt * this.silver_rate);
      if (this.frmpledge.get('silver_price').value !== 0) {
        const silverPrice = silverObj.sil_net_wgt * this.silver_rate;
        this.frmpledge.get('silver_charge_percent').setValue(0);
        this.frmpledge.get('regular_silver_percent').setValue(1);
        
        for (const charge of this.silver_rate_interest) {

          if ((silverPrice >= charge.chargeFromSilver) && (charge.chargeToSilver >= silverPrice)) {

            this.frmpledge.get('silver_charge_percent').setValue(charge.chargePercentSilver);

          }
        }

      }

      this.commoncalculation();
      this.regularcommoncalculation();


    }
  }
  commoncalculation() {
    if (this.frmpledge.get('gold_price').value !== 0) {
      const gold_charge = this.frmpledge.get('gold_charge_percent').value;
      const interestg = (Number(gold_charge) / 100) * this.frmpledge.get('gold_price').value;

      this.frmpledge.get('gold_interest').setValue(interestg);
    } else {
      this.frmpledge.get('gold_interest').setValue(0);
    }

    if (this.frmpledge.get('silver_price').value !== 0) {
      const silver_charge = this.frmpledge.get('silver_charge_percent').value;
      var interest = (Number(silver_charge) / 100) * this.frmpledge.get('silver_price').value;
      this.frmpledge.get('silver_interest').setValue(interest);
    } else {
      this.frmpledge.get('silver_interest').setValue(0);
    }


    const ser_chr = (this.frmpledge.get('service_charge').value) ? this.frmpledge.get('service_charge').value : 0;
    const tot_price = this.frmpledge.get('gold_price').value + this.frmpledge.get('silver_price').value;
    this.frmpledge.get('goldsilvertotalamt').setValue(tot_price);
    const redeem_amt = this.frmpledge.get('gold_interest').value + this.frmpledge.get('silver_interest').value + ser_chr;
    this.frmpledge.get('amount').setValue(tot_price - redeem_amt);

  }

  regularcommoncalculation() {
    if (this.frmpledge.get('gold_price').value !== 0) {
      const gold_charge = this.frmpledge.get('regular_gold_percent').value;
      const interestg = (Number(gold_charge) / 100) * this.frmpledge.get('gold_price').value;

      this.frmpledge.get('regular_gold_interest').setValue(interestg);
    } else {
      this.frmpledge.get('regular_gold_interest').setValue(0);
    }

    if (this.frmpledge.get('silver_price').value !== 0) {
      const silver_charge = this.frmpledge.get('regular_silver_percent').value;
      var interest = (Number(silver_charge) / 100) * this.frmpledge.get('silver_price').value;
      this.frmpledge.get('regular_silver_interest').setValue(interest);
    } else {
      this.frmpledge.get('regular_silver_interest').setValue(0);
    }


    const ser_chr = (this.frmpledge.get('service_charge').value) ? this.frmpledge.get('service_charge').value : 0;
    const tot_price = this.frmpledge.get('gold_price').value + this.frmpledge.get('silver_price').value;
    this.frmpledge.get('regular_gold_silver_amt').setValue(tot_price);
    const redeem_amt = this.frmpledge.get('regular_gold_interest').value + this.frmpledge.get('regular_silver_interest').value + ser_chr;
    this.frmpledge.get('regular_amount').setValue(tot_price - redeem_amt);
  }

  changegold(event) {

    this.frmpledge.get('gold_charge_percent').setValue(event);
    this.commoncalculation();
  }
  changesilver(event) {
    this.frmpledge.get('silver_charge_percent').setValue(event);
    this.commoncalculation();
  }



  onSubmit() {
    this.submitted = true;
    // console.log('this.frmpledge :-', this.frmpledge.value);
    // console.log('MainPledgeItems', this.MainPledgeItems);
    // return false;


    if (this.frmpledge.invalid) {
      return;
    }
    this.spinnerService.show();
    this.showLoading = true;
    //window.console.log(this.frmpledge);
    const formData = new FormData();
    formData.append('address_1', this.frmpledge.get('address_1').value);
    formData.append('address_2', this.frmpledge.get('address_2').value);
    formData.append('address_3', this.frmpledge.get('address_3').value);
    formData.append('amount', this.frmpledge.get('amount').value);
    formData.append('area', this.frmpledge.get('area').value);
    formData.append('bill_no_1', this.frmpledge.get('bill_no_1').value);
    formData.append('bill_no_2', this.frmpledge.get('bill_no_2').value);
    formData.append('bus_id', localStorage.getItem('bus_id'));
    formData.append('cust_name', this.frmpledge.get('cust_name').value);
    formData.append('photo_url', this.frmpledge.get('customerAvatar').value);
    formData.append('customer_id', this.frmpledge.get('customer_id').value);
    formData.append('date_of_bill', this.frmpledge.get('date_of_bill').value);
    formData.append('email', this.frmpledge.get('email').value);
    formData.append('father_name', this.frmpledge.get('father_name').value);
    formData.append('phone_no', this.frmpledge.get('phone_no').value);
    formData.append('gold_charge_percent', this.frmpledge.get('gold_charge_percent').value);
    formData.append('gold_interest', this.frmpledge.get('gold_interest').value);
    formData.append('gold_rate_interest', JSON.stringify(this.gold_rate_interest));
    formData.append('gold_amount', this.frmpledge.get('gold_amount').value);
    formData.append('gold_price', this.frmpledge.get('gold_price').value);
    formData.append('pin_code', this.frmpledge.get('pin_code').value);
    formData.append('silver_charge_percent', this.frmpledge.get('silver_charge_percent').value);
    formData.append('silver_interest', this.frmpledge.get('silver_interest').value);
    formData.append('silver_price', this.frmpledge.get('silver_price').value);
    formData.append('silver_amount', this.frmpledge.get('silver_amount').value);
    formData.append('silver_rate_interest', JSON.stringify(this.silver_rate_interest));
    formData.append('pledgeItems', JSON.stringify(this.MainPledgeItems));
    formData.append('pledge_id', this.frmpledge.get('pledge_id').value);
    formData.append('pledge_status', this.frmpledge.get('pledge_status').value);
    formData.append('service_charge', this.frmpledge.get('service_charge').value);
    formData.append('transaction_type', '2'); // full_amount : 1; interest : 2; partial : 3;
    formData.append('amount_principal', this.frmpledge.get('goldsilvertotalamt').value);
    const ser_chr = (this.frmpledge.get('service_charge').value) ? this.frmpledge.get('service_charge').value : 0;
    const redeem_amt = this.frmpledge.get('gold_interest').value + this.frmpledge.get('silver_interest').value + ser_chr;
    formData.append('amount_received', redeem_amt);
    const reg_ser_chr = (this.frmpledge.get('service_charge').value) ? this.frmpledge.get('service_charge').value : 0;
    const reg_redeem_amt = this.frmpledge.get('regular_gold_interest').value + this.frmpledge.get('regular_silver_interest').value + reg_ser_chr;
    formData.append('regular_amount_received', reg_redeem_amt);

    formData.append('regular_gold_interest', this.frmpledge.get('regular_gold_interest').value);
    formData.append('regular_silver_interest', this.frmpledge.get('regular_silver_interest').value);
    formData.append('regular_gold_percent', this.frmpledge.get('regular_gold_percent').value);
    formData.append('regular_silver_percent', this.frmpledge.get('regular_silver_percent').value);
    formData.append('regular_amount', this.frmpledge.get('regular_amount').value);
    formData.append('user_name', localStorage.getItem('user_name'));



    //window.console.log(formData);


    this._pledgeService.InsertPledge(formData)
      .subscribe(
        (data: any) => {
          this.ResetPledgeWindow();

          this.spinnerService.hide();
          // this.router.navigate(['/dashboard/']);

          // window.console.log(data);
          //alert(data);
          /*if(data.result=='NOK' && data.error==1){
            
          }else{
            
          }*/
          //this.UserList = data;
        },
        error => {
          alert('Error on Submition');
          this.showLoading = false;
        }
      );
    //   this.router.navigate(['/dashboard/']);
  }

  SearchUser() {
    const formData = new FormData();
    formData.append('find_user_input', this.frmpledge.get('find_user_input').value);
    this.showLoading = true;
    this._pledgeService.GetCustomerDetails(formData)
      .subscribe(
        (customer_data: any) => {
          //window.console.log(data);
          if (customer_data.length) {
            const data = customer_data[0];
            if (data != '') {
              this.frmpledge.get('cust_name').setValue(data.customer_name);
              this.frmpledge.get('father_name').setValue(data.customer_father);
              this.frmpledge.get('phone_no').setValue(data.customer_mobile);
              this.frmpledge.get('email').setValue(data.customer_email);
              this.frmpledge.get('area').setValue(data.area);
              this.frmpledge.get('pin_code').setValue(data.pincode);
              this.frmpledge.get('customer_id').setValue(data.customer_id);
              if (data.customer_address != '') {
                var address = data.customer_address.split(',');
                if (address[0] != undefined) this.frmpledge.get('address_1').setValue(address[0]);
                if (address[1] != undefined) this.frmpledge.get('address_2').setValue(address[1]);
                if (address[2] != undefined) this.frmpledge.get('address_3').setValue(address[2]);
              }
              this.customerPath = Globals.CUSTOMER_IMAGE + data.photo;
            } else {
              this.custFieldEnabled = false;
            }
            this.showLoading = false;
          }
        },
        error => {
          alert('Error on Submition');
          this.showLoading = false;
        }
      );
  }

  GetItemMaster() {
    this._pledgeService.GetItems()
      .subscribe(
        (data: any) => {
          // window.console.log(data);
          if (data.length > 0) {
            data.forEach(element => {
              let obj = { 'id': element.item_id, 'itemName': element.item_type + '-' + element.item_name, 'itemType': element.item_type };
              this.ItemList.push(obj);
            });
          }
        },
        error => {
          window.console.log(error);
          alert('Error on Submition');
        }
      );
  }

  GetBusSettings() {
    const busId = localStorage.getItem('bus_id');

    this._pledgeService.GetBusSettings(busId)
      .subscribe(
        (data: any) => {
          console.log('??', data);
          this.frmpledge.get('service_charge').setValue(0);
          this.frmpledge.get('silver_interest').setValue(0);
          this.frmpledge.get('gold_interest').setValue(0);
          const bill_ref_1 = data.find(data => data.bus_settings_name === 'bill_ref_1');
          const bill_ref_2 = data.find(data => data.bus_settings_name === 'bill_ref_2');

          const silver_rate = data.find(data => data.bus_settings_name === 'silver_rate');
          const silver_rate_interest = data.find(data => data.bus_settings_name === 'silver_rate_interest');


          const gold_rate = data.find(data => data.bus_settings_name === 'gold_rate');
          const gold_rate_interest = data.find(data => data.bus_settings_name === 'gold_rate_interest');

          if (bill_ref_1 != undefined) this.frmpledge.get('bill_no_1').setValue(bill_ref_1.bus_settings_value);
          if (bill_ref_2 != undefined) this.frmpledge.get('bill_no_2').setValue(bill_ref_2.bus_settings_value);

          this.silver_rate = 0;
          if (silver_rate != undefined) {
            this.silver_rate = parseInt(silver_rate.bus_settings_value);
            this.frmpledge.get('silver_amount').setValue(parseInt(silver_rate.bus_settings_value));

          }
          if (silver_rate_interest != undefined) {

            this.silver_rate_interest = JSON.parse(silver_rate_interest.bus_settings_value);

            this.frmpledge.get('silver_rate_interest').setValue(silver_rate_interest.bus_settings_value);
          }


          this.gold_rate = 0;
          if (gold_rate != undefined) {
            this.gold_rate = parseInt(gold_rate.bus_settings_value);
            this.frmpledge.get('gold_amount').setValue(parseInt(gold_rate.bus_settings_value));
            console.log('this.gold_rate', this.gold_rate)

          }
          if (gold_rate_interest != undefined) {
            this.gold_rate_interest = JSON.parse(gold_rate_interest.bus_settings_value);
            this.frmpledge.get('gold_rate_interest').setValue(gold_rate_interest.bus_settings_value);
           
          }

        },
        error => {
          window.console.log(error);
          alert('Error on Submition');
        }
      );
  }


  ResetPledgeWindow() {
    this.PledgeImgPath = '../../assets/img/add-prod.jpg';
    this.frmpledge.get('pledgeItemAvatar').setValue('');
    this.frmpledge.get('selectedPledgeItem').setValue('');
    this.frmpledge.get('no_of_pcs').setValue('');
    this.frmpledge.get('wgt_gms').setValue('');
    this.frmpledge.get('waste_gms').setValue('');
    this.frmpledge.get('net_wgt').setValue('');
  }

  newCustomer() {
    this.custFieldEnabled = false;
  }

  AddItem() {
    this.ResetPledgeWindow();
    this.infoModal.show();
  }

  formatDate() {
    var d = new Date(),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }
}
