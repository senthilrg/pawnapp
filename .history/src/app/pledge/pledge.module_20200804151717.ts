import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { HttpClient,HttpClientModule } from '@angular/common/http';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { PledgeService } from './pledge.services';
import { AppBootstrapModule } from "../app-bootstarp/app-bootstrap.module";

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers:[PledgeService],
  declarations: []
})
export class PledgeModule { }