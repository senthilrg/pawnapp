import { environment } from '../src/environments/environment'
export const Globals = Object.freeze({
    BASE_API_URL: environment.E_BASE_API_URL,
    CI_BASE_URL : environment.E_BASE_API_URL,  //http://localhost/pawn_ci_api/   //environment.E_BASE_API_URL
    PER_PAGE:environment.PER_PAGE,
    CLIENT_SESSION:10,
    CUSTOMER_IMAGE: environment.E_BASE_API_URL+"upload/image/customer/",  // environment.E_BASE_API_URL+"upload/image/customer/"     //"http://localhost/pawn_ci_api/upload/image/customer/"
    PLEDGE_IMAGE: environment.E_BASE_API_URL+"upload/image/pledges/" ,      // environment.E_BASE_API_URL+"upload/image/pledges/"       //"http://localhost/pawn_ci_api/upload/image/pledges/"
    BUS_IMAGE: environment.E_BASE_API_URL+"upload/image/business/",
    VERSION:'1.0.0.8'
});