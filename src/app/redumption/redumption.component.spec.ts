import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedumptionComponent } from './redumption.component';

describe('RedumptionComponent', () => {
  let component: RedumptionComponent;
  let fixture: ComponentFixture<RedumptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedumptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedumptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
