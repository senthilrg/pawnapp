import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { HttpClient,HttpClientModule } from '@angular/common/http';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { GenSettingsService } from './gensettings.services';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers:[GenSettingsService],
  declarations: []
})
export class GenSettingsModule { }