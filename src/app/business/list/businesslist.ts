export interface IBussinessList {
    business_id : string,
    business_name : string,
    user_id: string,
    business_email : string,
    mobile_no :string,
    address_1_1:string,
    address_1_2 :string,
    address_1_3:string,
    PAN :string,
    GST:string,
    CIN :string,
    TAN :string,
    Photo :string,
    created_datetime :string
}