import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { HttpClient,HttpClientModule } from '@angular/common/http';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import {BusinessListService}  from './businesslist.services';
import {BusinessListComponent} from './businesslist.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers:[BusinessListService],
  declarations: [ ]
})
export class BusinesslistModule { }