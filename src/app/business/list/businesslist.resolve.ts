import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot,Router } from '@angular/router';
import { BusinessListService } from './businesslist.services';
import {IBussinessList} from './businesslist';
import { Observable,empty } from 'rxjs';

@Injectable()
export class BusinessResolve implements Resolve<any> {

  constructor(private _businessService: BusinessListService,private router: Router) {}

 /* resolve(route: ActivatedRouteSnapshot) {
    return this._roleService.GetAllRoles(localStorage.getItem('sessionId'));
  }*/

  resolve(route: ActivatedRouteSnapshot): Observable <any> {

    return this._businessService.GetBusiness(localStorage.getItem("user_id"));
  }
}