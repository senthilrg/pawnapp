import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PledgeReportComponent } from './pledge-report/pledge-report.component';

const routes: Routes = [
  {
    path: '',
    component: PledgeReportComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
