import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-view-register',
  templateUrl: './view-register.component.html',
  styleUrls: ['./view-register.component.scss']
})
export class ViewRegisterComponent implements OnInit {

  now: number;
  public register;

  constructor(public bsModalRef: BsModalRef) {
    this.now = Date.now();
   }

  ngOnInit() {
    console.log('register', this.register)
  }

}
