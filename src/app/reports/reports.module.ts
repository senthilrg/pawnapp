import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PledgeReportComponent } from './pledge-report/pledge-report.component';
import { ReportsRoutingModule } from './reports-routing.module';
import { AppBootstrapModule } from "../app-bootstarp/app-bootstrap.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {ReportService} from './shared/report.service';
import { TransactionPipe } from '../helper/transaction.pipe';
import { PledgePipe } from '../helper/pledge.pipe';
import { ViewRegisterComponent } from './view-register/view-register.component';

@NgModule({
  declarations: [PledgeReportComponent, TransactionPipe, PledgePipe, ViewRegisterComponent],
  imports: [
    CommonModule,
    ReportsRoutingModule,
    FormsModule, ReactiveFormsModule,
    AppBootstrapModule,
  ],
  providers: [ReportService],
  entryComponents: [ViewRegisterComponent]
})
export class ReportsModule { }
