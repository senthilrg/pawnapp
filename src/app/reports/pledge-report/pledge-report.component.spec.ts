import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PledgeReportComponent } from './pledge-report.component';

describe('PledgeReportComponent', () => {
  let component: PledgeReportComponent;
  let fixture: ComponentFixture<PledgeReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PledgeReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PledgeReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
