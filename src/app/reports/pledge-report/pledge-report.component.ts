import { Component, OnInit } from '@angular/core';
import { ReportService } from '../shared/report.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import {ViewRegisterComponent} from '../view-register/view-register.component';


@Component({
  selector: 'app-pledge-report',
  templateUrl: './pledge-report.component.html',
  styleUrls: ['./pledge-report.component.scss']
})
export class PledgeReportComponent implements OnInit {

  showLoading: boolean = true;
  public startdate;
  public enddate;
  public starttime = '';
  public endtime = '';
  public customer = '';
  public reportType = 1;
  public MainPledge = [];
  public MainRedemptions = [];
  public MainExpense = [];
  public MainCashInHand = [];
  public TransactionArray = [];
  bsModalRefdetails: BsModalRef;
  //public Main

  constructor(private report: ReportService, private modalService: BsModalService) {
    this.showLoading = true;
  }

  ngOnInit() {
    this.showLoading = false;
    this.searchFilter();
  }
  changeExpstart(event: any) {
    if (event != null) {
      const expdate = event.setHours(0, 0, 0, 0);
      // var s = new Date(expdate).getTime();
      var d = new Date(expdate), month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      [year, month, day].join('-');
      console.log([year, month, day].join('-'), 'dates');
      this.starttime = [year, month, day].join('-');
    }
  }

  changeExpend(event: any) {
    if (event != null) {
      const expdate = event.setHours(23, 59, 59, 0);
      // var s = new Date(expdate).getTime();
      var d = new Date(expdate), month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
      [year, month, day].join('-');
      console.log([year, month, day].join('-'), 'dates');
      this.endtime = [year, month, day].join('-');
    }
  }
  searchFilter() {
    this.MainPledge =[];
    this.MainExpense = [];
    this.MainRedemptions = [];
    this.MainCashInHand = []

    const filterobj = {
      bus_id: localStorage.getItem("bus_id"),
      reporttype: String(this.reportType),
      fromdate: (this.startdate) ? this.starttime : this.formatDate(),
      todate: (this.enddate) ? this.endtime : this.formatDate(),
      customer: (this.customer) ? this.customer : null
    };
  
    this.report.searchFilter(filterobj).subscribe(
      (data: any) => {
        console.log('data', data)
        if (data.length) {
          if (this.reportType == 1) {
              this.MainPledge = data[0];
              this.MainExpense = data[1];
              this.MainRedemptions = data[2];
          } else if (this.reportType == 2) {
            this.MainPledge = data;
          } else if (this.reportType == 3){
            this.MainExpense = data;
          }else if (this.reportType == 4) {
            this.MainRedemptions = data;
          }else if (this.reportType == 5) {
            this.MainCashInHand = data;
          }
        }
      },
      error => {
        window.console.log(error);
        alert("Error on Submition");
      }
    );
    // console.log('filterobj', filterobj);
  }
  formatDate() {
    var d = new Date(),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }
  viewPledge(pledge) {
    this.report.viewPledge(pledge.pledge_id).subscribe(
      (data: any) => {
        console.log('data', data)
        if (data.length) {
           this.TransactionArray = data;
        }
      },
      error => {
        window.console.log(error);
        alert("Error on Submition");
      }
    );
  }

  viewRegister(register) {
    const initialStateProd = {
      title: 'View Register',
      class: 'modal-lg',
      backdrop: true,
      ignoreBackdropClick: true,
      initialState: {
        register: register,
        
      }
    };
    this.bsModalRefdetails = this.modalService.show(ViewRegisterComponent, initialStateProd);
  
  }
  


}
