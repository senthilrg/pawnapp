import { Injectable } from '@angular/core';
import { HttpClient ,HttpHeaders ,HttpParams, HttpResponse} from '@angular/common/http';
import { Globals } from '../../../global';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(private http: HttpClient) { }
  searchFilter(filterobj : any) {
    console.log('filterobj service', filterobj);
    const formData = new FormData();

    formData.append("bus_id", filterobj.bus_id);
    formData.append("reporttype", filterobj.reporttype);
    formData.append('fromdate', filterobj.fromdate);
    formData.append('todate', filterobj.todate);
    formData.append('customer', filterobj.customer);
    formData.append('varient', localStorage.getItem('user_varient'));
    let headerOptions = new HttpHeaders();
    headerOptions.append('Access-Control-Allow-Credentials', 'true');
    headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
    headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    return this.http.post(Globals.CI_BASE_URL+"ReportList",formData,
    {headers:headerOptions});
  }
  viewPledge(id : any) {
    const varient =  localStorage.getItem('user_varient')
    let headerOptions = new HttpHeaders();

        headerOptions.append('Access-Control-Allow-Credentials', 'true');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        return this.http.get(Globals.CI_BASE_URL + "ListTransaction/?pledge_id=" + id + '&varient=' + varient+'&rand='+ Math.random(), { headers: headerOptions });
  }
}
