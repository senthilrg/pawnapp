import { Injectable } from '@angular/core';
import { HttpClient ,HttpHeaders ,HttpParams, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map, filter, catchError, mergeMap } from 'rxjs/operators'
//import {IUserList} from './users';
import { Globals } from '../../../global';
import { RequestOptionsArgs, RequestMethod, RequestOptions } from '@angular/http';
@Injectable()

export class PledgelistService{

    constructor(private http: HttpClient){ 
    }

    GetPledges(bus_id:string,bill:string){
        let headerOptions = new HttpHeaders();
        headerOptions.append('Access-Control-Allow-Credentials', 'true');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

        let params = new HttpParams();
        params = params.append('bus_id', bus_id);
        params = params.append('bill', bill);
        return this.http.get(Globals.CI_BASE_URL +"PledgeList/?rand="+ Math.random(),{headers:headerOptions,params: params});
    }
    
}