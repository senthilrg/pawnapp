import { Component, OnInit ,ViewChild} from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import { PledgelistService } from './pledgelist.services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Globals } from '../../../global';

@Component({
  selector: 'app-user-profile',
  templateUrl: './pledgelist.component.html',
  styleUrls: ['./pledgelist.component.scss']
})
export class PledgelistComponent implements OnInit {
  
  showLoading:boolean = true;
  bill:string = "";
  PledgeList = [];
  constructor(private router:Router,private route:ActivatedRoute,private formBuilder: FormBuilder,private _pledgeService:PledgelistService) { 
    this.showLoading = true;
    this.bill = this.route.snapshot.paramMap.get('bill');
  }
  
  managePledge(bill_no:string){
    this.router.navigate(['/pledge/manage/'+bill_no]);
  }

  ngOnInit() {
    this.showLoading = false;
    this._pledgeService.GetPledges(localStorage.getItem("bus_id"),this.bill)
    .subscribe(
      (data:any)=>{
        //window.console.log(data);
        this.PledgeList = data;
      },
      error=>{
        window.console.log(error);
        alert("Error on Submition");            
      }
    );
    
  }

}
