import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PledgelistComponent } from './pledgelist.component';

describe('PledgeComponent', () => {
  let component: PledgelistComponent;
  let fixture: ComponentFixture<PledgelistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PledgelistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PledgelistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
