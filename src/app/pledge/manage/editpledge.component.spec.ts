import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPledgeComponent } from './editpledge.component';

describe('PledgeComponent', () => {
  let component: EditPledgeComponent;
  let fixture: ComponentFixture<EditPledgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPledgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPledgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
