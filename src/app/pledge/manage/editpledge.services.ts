import { Injectable } from '@angular/core';
import { HttpClient ,HttpHeaders ,HttpParams, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map, filter, catchError, mergeMap } from 'rxjs/operators'
//import {IUserList} from './users';
import { Globals } from '../../../global';
import { RequestOptionsArgs, RequestMethod, RequestOptions } from '@angular/http';
@Injectable()

export class EditPledgeService{
    private _url: string = Globals.BASE_API_URL;

    constructor(private http: HttpClient){ 
      this._url = this._url + "pledge/";
    }     

    UpdatePledge(value){
        //window.console.log(value);
        let headerOptions = new HttpHeaders();
        headerOptions.append('Access-Control-Allow-Credentials', 'true');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

        return this.http.post(Globals.CI_BASE_URL+"PledgeEdit", value, {
            headers: headerOptions,
        });
    }

    GetCustomerDetails(value){
        //window.console.log(value);
        let headerOptions = new HttpHeaders();
        headerOptions.append('Access-Control-Allow-Credentials', 'true');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

        return this.http.post(Globals.CI_BASE_URL+"CustomerList", value, {
            headers: headerOptions,
        });
    }

    GetItems() {
        let headerOptions = new HttpHeaders();
        headerOptions.append('Access-Control-Allow-Credentials', 'true');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        return this.http.get(Globals.CI_BASE_URL + "itemList/?rand="+ Math.random(), { headers: headerOptions });
    }

    GetBusSettings(bus_id: string) {
        let headerOptions = new HttpHeaders();
        headerOptions.append('Access-Control-Allow-Credentials', 'true');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        return this.http.get(Globals.CI_BASE_URL + "GetBusinessSettings/?bus_id=" + bus_id+'&rand='+ Math.random(), { headers: headerOptions });
    }

    GetPledgeData(id : string) {
        let headerOptions = new HttpHeaders();
        headerOptions.append('Access-Control-Allow-Credentials', 'true');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        return this.http.get(Globals.CI_BASE_URL+"PledgeListInfo/?bill_no="+id+'&rand='+ Math.random(),{headers:headerOptions});
    }

    GetPledgeItem(id : string) {
        // http://actechsoftware.com/pawnservice/PledgeItems/GetPledgedItems/?pledge_id=2
        let headerOptions = new HttpHeaders();
        headerOptions.append('Access-Control-Allow-Credentials', 'true');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        return this.http.get(Globals.CI_BASE_URL+"PledgeItemList/?pledge_id="+id +'&rand='+ Math.random(),{headers:headerOptions});
    }

    GetTransaction(id : string) {
        // http://actechsoftware.com/pawnservice/PledgeItems/GetPledgedItems/?pledge_id=2
        let headerOptions = new HttpHeaders();
        headerOptions.append('Access-Control-Allow-Credentials', 'true');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        return this.http.get(Globals.CI_BASE_URL+"TransactionList/?pledge_id="+id +'&rand='+ Math.random(),{headers:headerOptions});
    }

    updatetransactions(value) {
        let headerOptions = new HttpHeaders();
        headerOptions.append('Access-Control-Allow-Credentials', 'true');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

        return this.http.post(Globals.CI_BASE_URL+"UpdateTransaction", value, {
            headers: headerOptions,
        });
    }
}