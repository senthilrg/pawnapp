import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { HttpClient,HttpClientModule } from '@angular/common/http';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { EditPledgeService } from './editpledge.services';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers:[EditPledgeService],
  declarations: []
})
export class EditPledgeModule { }