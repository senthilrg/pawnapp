import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EditPledgeService } from './editpledge.services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Globals } from '../../../global';
import { distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-user-profile',
  templateUrl: './editpledge.component.html',
  styleUrls: ['./editpledge.component.scss']
})
export class EditPledgeComponent implements OnInit {
  frmpledge: FormGroup;
  submitted: Boolean = false;
  customerPath: string;
  selectedFile: File;
  uploadedFile: string = "";
  custFieldEnabled: Boolean = true;
  PledgeImgPath: string = "";
  dropdownSettings = {};
  ItemList = [];
  selectedPledgeItm: string = "";
  MainPledgeItems = [];
  BackupPledgeItem = [];
  PledgeImgName: string = "";
  showLoading: boolean = true;
  tot_pcs: number = 0;
  tot_wgt: number = 0;
  tot_wst: number = 0;
  tot_net_wgt: number = 0;
  gold_rate: number = 0;
  silver_rate: number = 0;
  @ViewChild('infoModal') infoModal: ModalDirective;
  public gold_rate_interest = [];
  public silver_rate_interest = [];

  constructor(private router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder, private _pledgeService: EditPledgeService) {
    this.customerPath = "../../../assets/img/add-user.png";
    this.PledgeImgPath = "../../assets/img/add-prod.jpg";
    this.showLoading = true;
    //this.GetBusSettings();
  }


  ngOnInit() {

    this.route.params.subscribe(params => {
      const id = params['bill'];
      console.log('id', id)
      if (id) {
        this.getPledge(id);
      }
    });

    this.GetItemMaster();
    
    /*$(".item_img_holder img").on("click",function(){
      $(this).next(".clsImgProd").click();
    });*/


    this.frmpledge = this.formBuilder.group({
      bill_no_1: ['', Validators.required],
      bill_no_2: ['', Validators.required],
      date_of_bill: ['', Validators.required],
      cust_name: ['', Validators.required],
      father_name: ['', Validators.required],
      phone_no: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      area: ['', Validators.required],
      pin_code: ['', Validators.required],
      address_1: [''],
      address_2: [''],
      address_3: [''],
      price: [''],
      roi: [''],
      service_charge: [0],
      interest: [''],
      amount: ['', Validators.required],
      customerAvatar: [''],
      pledge_id: [0],
      pledge_status : [1],
      pledgeItemAvatar: [''],
      find_user_input: [''],
      customer_id: [0],
      item_name: [''],
      no_of_pcs: [''],
      wgt_gms: [''],
      waste_gms: [''],
      net_wgt: [''],
      selectedPledgeItem: [''],
      selectedPledgeName: [''],
      selectedPledgeType: [''],
      gold_price: [''],
      gold_rate_interest: [''],
      gold_interest: [''],
      gold_service_charge: [''],
      gold_charge_percent: [''],
      gold_amount: [''],
      silver_amount: [''],
      silver_price: [''],
      silver_rate_interest: [''],
      silver_interest: [''],
      silver_service_charge: [''],
      silver_charge_percent: [''],
      goldsilvertotalamt: []
    });

    this.dropdownSettings = {
      singleSelection: true,
      text: "Select",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: false,
      classes: "myclass custom-class"
    };
    this.frmpledge.get("date_of_bill").setValue(this.formatDate());
    this.showLoading = false;
    this.formControlValueChanged();
  }

  get f() { return this.frmpledge.controls; }
  formControlValueChanged() {

    this.frmpledge.get('service_charge').valueChanges.pipe(distinctUntilChanged()).subscribe(
      (mode: number) => {
        console.log('mode', mode)
        this.commoncalculation();
      });
   
  

  // const  this.usersForm.get('')
}

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
    this.uploadedFile = this.selectedFile.name;

    var reader = new FileReader();

    this.frmpledge.get('customerAvatar').setValue(this.selectedFile);
    reader.onload = (event: any) => {
      //window.console.log(event.target.result);
      //window.console.log(event);
      this.customerPath = event.target.result;
    }
    reader.readAsDataURL(event.target.files[0])
  }

  onPledgeChanged(event) {
    this.frmpledge.get('pledgeItemAvatar').setValue(event.target.files[0]);
    // window.console.log(event.target.files[0].name);
    this.PledgeImgName = event.target.files[0].name;
    var reader = new FileReader();

    reader.onload = (event: any) => {
      this.PledgeImgPath = event.target.result;
    }
    reader.readAsDataURL(event.target.files[0])
  }

  onPledgeItemSelect(item: any) {
    this.selectedPledgeItm = item.id;
    this.frmpledge.get('selectedPledgeName').setValue(item.itemName);
    this.frmpledge.get('selectedPledgeType').setValue(item.itemType);
  }
  OnPledgeItemDeSelect(item: any) {
    this.selectedPledgeItm = "";
  }

  AddPledgeItem() {
    //alert("ASDF");
    const pledgeId = this.frmpledge.get('selectedPledgeItem').value;
    let itm = {
      "imgPath": this.PledgeImgPath,
      "imgFile": this.PledgeImgName,
      "pledgeItm": (pledgeId) ? pledgeId[0]['id'] : 0,
      "itemName": this.frmpledge.get('selectedPledgeName').value,
      'itemType': this.frmpledge.get('selectedPledgeType').value,
      "no_of_pcs": this.frmpledge.get('no_of_pcs').value,
      "wgt_gms": this.frmpledge.get('wgt_gms').value,
      "waste_gms": this.frmpledge.get('waste_gms').value,
      "net_wgt": this.frmpledge.get('net_wgt').value,
      "is_deleted" : 0,
      "pledge_item_id": 0
    }
    this.MainPledgeItems.push(itm);
    this.BackupPledgeItem.push(itm);
    //window.console.log(this.MainPledgeItems);
    this.infoModal.hide();

    this.reCalcPledgeItems();
  }

  removePledgeItm(index) {
    let newObject = Object.assign([], this.MainPledgeItems);
    newObject[index].is_deleted = 1;
   
    console.log('newObject', newObject);
 this.BackupPledgeItem = newObject;
    this.MainPledgeItems.splice(index, 1);
    this.reCalcPledgeItems();
  }

  reCalcPledgeItems() {
    this.tot_pcs = 0;
    this.tot_net_wgt = 0;
    this.tot_wgt = 0;
    this.tot_wst = 0;
    const silverObj = {
      sil_pcs: 0,
      sil_wgt: 0,
      sil_wst: 0,
      sil_net_wgt: 0
    };
    const goldObj = {
      gol_pcs: 0,
      gol_wgt: 0,
      gol_wst: 0,
      gol_net_wgt: 0
    };

    if (this.MainPledgeItems.length > 0) {

      this.MainPledgeItems.forEach(element => {
        if (element.itemType == 'silver') {

          silverObj.sil_net_wgt = silverObj.sil_net_wgt + parseInt(element.net_wgt);
          silverObj.sil_pcs = silverObj.sil_pcs + parseInt(element.no_of_pcs);
          silverObj.sil_wgt = silverObj.sil_wgt + parseInt(element.wgt_gms);
          silverObj.sil_wst = silverObj.sil_wst + parseInt(element.waste_gms);
        } else if (element.itemType == 'gold') {
          goldObj.gol_net_wgt = goldObj.gol_net_wgt + parseInt(element.net_wgt);
          goldObj.gol_pcs = goldObj.gol_pcs + parseInt(element.no_of_pcs);
          goldObj.gol_wgt = goldObj.gol_wgt + parseInt(element.wgt_gms);
          goldObj.gol_wst = goldObj.gol_wst + parseInt(element.waste_gms);
        }
        this.tot_pcs = this.tot_pcs + parseInt(element.no_of_pcs);
        this.tot_wgt = this.tot_wgt + parseInt(element.wgt_gms);
        this.tot_wst = this.tot_wst + parseInt(element.waste_gms);
        this.tot_net_wgt = this.tot_net_wgt + parseInt(element.net_wgt);
      });

       
      this.frmpledge.get("gold_price").setValue(goldObj.gol_net_wgt * this.gold_rate);
      this.frmpledge.get("gold_charge_percent").setValue(0);
      if (this.frmpledge.get("gold_price").value !== 0) {
        const goldPrice = goldObj.gol_net_wgt * this.gold_rate;
       
        for (const charge of this.gold_rate_interest) {

          if ((goldPrice >= charge.chargeFromGold) && (charge.chargeToGold >= goldPrice)) {

            this.frmpledge.get("gold_charge_percent").setValue(charge.chargePercentGold);

          }
        }

      }

      this.frmpledge.get("silver_price").setValue(silverObj.sil_net_wgt * this.silver_rate);
      this.frmpledge.get("silver_charge_percent").setValue(0);
      if (this.frmpledge.get("silver_price").value !== 0) {
        const silverPrice = silverObj.sil_net_wgt * this.silver_rate;
        
        for (const charge of this.silver_rate_interest) {

          if ((silverPrice >= charge.chargeFromSilver) && (charge.chargeToSilver >= silverPrice)) {

            this.frmpledge.get("silver_charge_percent").setValue(charge.chargePercentSilver);

          }
        }

      }

      this.commoncalculation();


    }
  }

  commoncalculation() {
    if (this.frmpledge.get("gold_price").value !== 0) {
      const gold_charge = this.frmpledge.get("gold_charge_percent").value;
      const interestg = (Number(gold_charge) / 100) * this.frmpledge.get("gold_price").value;

      this.frmpledge.get("gold_interest").setValue(interestg);
    } else {
      this.frmpledge.get("gold_interest").setValue(0);
    }

    if (this.frmpledge.get("silver_price").value !== 0) {
      const silver_charge = this.frmpledge.get("silver_charge_percent").value;
      var interest = (Number(silver_charge) / 100) * this.frmpledge.get("silver_price").value;
      this.frmpledge.get("silver_interest").setValue(interest);
    } else {
      this.frmpledge.get("silver_interest").setValue(0);
    }


   const ser_chr = (this.frmpledge.get("service_charge").value) ? this.frmpledge.get("service_charge").value : 0;
    const tot_price = this.frmpledge.get("gold_price").value + this.frmpledge.get("silver_price").value;
    this.frmpledge.get('goldsilvertotalamt').setValue(tot_price);
    const redeem_amt = this.frmpledge.get("gold_interest").value + this.frmpledge.get("silver_interest").value + ser_chr;
      this.frmpledge.get("amount").setValue(tot_price - redeem_amt);

  }

  onSubmit() {
    this.submitted = true;
    console.log('this.frmpledge form value', this.frmpledge.value);
    console.log('MainPledgeItems', this.MainPledgeItems);
    // return false;

    //  alert(this.frmpledge.invalid);

    if (this.frmpledge.invalid) {
      return;
    }
    this.showLoading = true;
    //window.console.log(this.frmpledge);
    const formData = new FormData();
    formData.append("address_1", this.frmpledge.get("address_1").value);
    formData.append("address_2", this.frmpledge.get("address_2").value);
    formData.append('address_3', this.frmpledge.get("address_3").value);
    formData.append('amount', this.frmpledge.get("amount").value);
    formData.append("area", this.frmpledge.get("area").value);
    formData.append("bill_no_1", this.frmpledge.get("bill_no_1").value);
    formData.append("bill_no_2", this.frmpledge.get("bill_no_2").value);
    formData.append("bus_id", localStorage.getItem("bus_id"));
    formData.append("cust_name", this.frmpledge.get("cust_name").value);
    formData.append('photo_url', this.frmpledge.get("customerAvatar").value);
    formData.append("customer_id", this.frmpledge.get("customer_id").value);
    formData.append("date_of_bill", this.frmpledge.get("date_of_bill").value);
    formData.append("email", this.frmpledge.get("email").value);
    formData.append("father_name", this.frmpledge.get("father_name").value);
    formData.append("phone_no", this.frmpledge.get("phone_no").value);
    formData.append("gold_charge_percent", this.frmpledge.get("gold_charge_percent").value);
    formData.append("gold_interest", this.frmpledge.get("gold_interest").value);
    formData.append("gold_rate_interest", JSON.stringify(this.gold_rate_interest));
    formData.append("gold_amount", this.frmpledge.get("gold_amount").value);
    formData.append("gold_price", this.frmpledge.get("gold_amount").value);
    formData.append("pin_code", this.frmpledge.get("pin_code").value);
    formData.append("silver_charge_percent", this.frmpledge.get("silver_charge_percent").value);
    formData.append("silver_interest", this.frmpledge.get("silver_interest").value);
    formData.append("silver_amount", this.frmpledge.get("silver_amount").value);
    formData.append("silver_price", this.frmpledge.get("silver_amount").value);
    formData.append("silver_rate_interest", JSON.stringify(this.silver_rate_interest));
    formData.append('pledgeItems', JSON.stringify(this.BackupPledgeItem));
    formData.append("pledge_id", this.frmpledge.get("pledge_id").value);
    formData.append("pledge_status", this.frmpledge.get("pledge_status").value);
    formData.append("service_charge", this.frmpledge.get("service_charge").value);
    formData.append("transaction_type", '2'); // full_amount : 1; interest : 2; partial : 3;
    formData.append("amount_principal", this.frmpledge.get("goldsilvertotalamt").value );
    const ser_chr = (this.frmpledge.get("service_charge").value) ? this.frmpledge.get("service_charge").value : 0;
    const redeem_amt = this.frmpledge.get("gold_interest").value + this.frmpledge.get("silver_interest").value + ser_chr;
    formData.append("amount_received", redeem_amt);
    formData.append('user_name', localStorage.getItem('user_name'));

    //window.console.log(formData);


    this._pledgeService.UpdatePledge(formData)
      .subscribe(
        (data: any) => {
          console.log('data', data)
          this.ResetPledgeWindow();

          this.showLoading = false;

          // window.console.log(data);
          //alert(data);
          /*if(data.result=="NOK" && data.error==1){
            
          }else{
            
          }*/
          //this.UserList = data;
        },
        error => {
          console.log('error', error)
         // alert("Error on Submition");
          this.showLoading = false;
        }
      );
     // this.router.navigate(['/dashboard/']);
  }

  SearchUser() {
    const formData = new FormData();
    formData.append("find_user_input", this.frmpledge.get("find_user_input").value);
    this.showLoading = true;
    this._pledgeService.GetCustomerDetails(formData)
      .subscribe(
        (data: any) => {
          if (data != "") {
            this.frmpledge.get("cust_name").setValue(data.customer_name);
            this.frmpledge.get("father_name").setValue(data.customer_father);
            this.frmpledge.get("phone_no").setValue(data.customer_mobile);
            this.frmpledge.get("email").setValue(data.customer_email);
            this.frmpledge.get("area").setValue(data.area);
            this.frmpledge.get("pin_code").setValue(data.pincode);
            this.frmpledge.get("customer_id").setValue(data.customer_id);
            if (data.customer_address != "") {
              var address = data.customer_address.split(",");
              if (address[0] != undefined) this.frmpledge.get("address_1").setValue(address[0]);
              if (address[1] != undefined) this.frmpledge.get("address_2").setValue(address[1]);
              if (address[2] != undefined) this.frmpledge.get("address_3").setValue(address[2]);
            }
            this.customerPath = Globals.CUSTOMER_IMAGE + data.photo;
          } else {
            this.custFieldEnabled = false;
          }
          this.showLoading = false;
          /*if(data.result=="NOK" && data.error==1){
            
          }else{
            
          }*/
          //this.UserList = data;
        },
        error => {
          alert("Error on Submition");
          this.showLoading = false;
        }
      );
  }

  GetItemMaster() {
    this._pledgeService.GetItems()
      .subscribe(
        (data: any) => {
          // window.console.log(data);
          if (data.length > 0) {
            data.forEach(element => {
              let obj = { "id": element.item_id, "itemName": element.item_type + "-" + element.item_name, "itemType": element.item_type };
              this.ItemList.push(obj);
            });
          }
        },
        error => {
          window.console.log(error);
          alert("Error on Submition");
        }
      );
  }

  getPledge(id) {
    this._pledgeService.GetPledgeData(id).subscribe(
      (data: any) => {
        if (data.length) {
          console.log('pledge data ', data)
          const pledgeVal = data[0];

          this.frmpledge.get("pledge_id").setValue(pledgeVal.pledge_id);
          this.pledgeItem(pledgeVal.pledge_id)
          if (pledgeVal.pledge_bill_no != undefined) {
            var billarray = pledgeVal.pledge_bill_no.split("-");

            this.frmpledge.get("bill_no_1").setValue(billarray[0]);
            this.frmpledge.get("bill_no_2").setValue(billarray[1]);
          }

          this.frmpledge.get("cust_name").setValue(pledgeVal.customer_name);
          this.frmpledge.get("father_name").setValue(pledgeVal.customer_father);
          this.frmpledge.get("phone_no").setValue(pledgeVal.customer_mobile);
          this.frmpledge.get("email").setValue(pledgeVal.customer_email);
          this.frmpledge.get("pin_code").setValue(pledgeVal.pincode);
          this.frmpledge.get("area").setValue(pledgeVal.area);
          this.frmpledge.get("customer_id").setValue(pledgeVal.customer_id);


          if (pledgeVal.customer_address) {
            var res = pledgeVal.customer_address.split(",");
            const add1 = res[0] ? res[0] : '';
            const add2 = res[1] ? res[1] : '';
            const add3 = res[2] ? res[2] : '';
            this.frmpledge.get("address_1").setValue(add1);
            this.frmpledge.get("address_2").setValue(add2);
            this.frmpledge.get("address_3").setValue(add3);
          }

          this.gold_rate = Number(pledgeVal.gold_pledge_price);
          this.frmpledge.get("gold_amount").setValue(Number(pledgeVal.gold_pledge_price));
          this.frmpledge.get("gold_price").setValue(Number(pledgeVal.gold_pledge_price));
          this.frmpledge.get("gold_interest").setValue(Number(pledgeVal.gold_interest));
          this.gold_rate_interest = JSON.parse(pledgeVal.gold_roi);
          this.frmpledge.get("gold_rate_interest").setValue(JSON.parse(pledgeVal.gold_roi));
          this.frmpledge.get("gold_charge_percent").setValue(Number(pledgeVal.gold_service_charge));

          this.silver_rate = Number(pledgeVal.silver_pledge_price);
          this.frmpledge.get("silver_amount").setValue(Number(pledgeVal.silver_pledge_price));
          this.frmpledge.get("silver_price").setValue(Number(pledgeVal.silver_pledge_price));
          this.frmpledge.get("silver_interest").setValue(Number(pledgeVal.silver_interest));
          this.silver_rate_interest = JSON.parse(pledgeVal.silver_roi);
          this.frmpledge.get("silver_rate_interest").setValue(JSON.parse(pledgeVal.silver_roi));
          this.frmpledge.get("silver_charge_percent").setValue(Number(pledgeVal.silver_service_charge));
          this.frmpledge.get("service_charge").setValue(Number(pledgeVal.service_charge));
          


          this.customerPath = Globals.CUSTOMER_IMAGE + pledgeVal.photo;

        }
      });
  }

  pledgeItem(id) {
    this.MainPledgeItems = [];
    this._pledgeService.GetPledgeItem(id).subscribe(async (data: any) => {

      if (data.length) {
        const itemData = this.ItemList.filter(role => data.find(item => item.pledge_item == role.id));
        for (const item of data) {

          const pledgeitem = Globals.PLEDGE_IMAGE + item.pledge_image;
          console.log('pledgeitem', item)
          const typeItem = itemData.find(itemtype => itemtype.id == item.pledge_item);
          let itm = {
            "imgPath": pledgeitem,
            "itemType": (typeItem) ? typeItem.itemType : null,
            "itemName": (typeItem) ? typeItem.itemName : null,
            "imgFile": item.pledge_image,
            "pledgeItm": item.pledge_item,
            "no_of_pcs": item.no_of_pcs,
            "wgt_gms": item.no_of_wgts,
            "waste_gms": item.wgt_grms,
            "net_wgt": item.net_wgts,
            "pledge_item_id": item.pledge_item_id,
            "is_deleted": item.is_deleted,
          }
          if (item.is_deleted == '0') {
            this.MainPledgeItems.push(itm);
          }
         
        }
        const conditionBase = await this.MainPledgeItems;
        this.reCalcPledgeItems();

      }

    });

  }

  ResetPledgeWindow() {
    this.PledgeImgPath = "../../assets/img/add-prod.jpg";
    this.frmpledge.get('pledgeItemAvatar').setValue("");
    this.frmpledge.get('selectedPledgeItem').setValue("");
    this.frmpledge.get('no_of_pcs').setValue("");
    this.frmpledge.get('wgt_gms').setValue("");
    this.frmpledge.get('waste_gms').setValue("");
    this.frmpledge.get('net_wgt').setValue("");
  }

  newCustomer() {
    this.custFieldEnabled = false;
  }

  AddItem() {
    this.ResetPledgeWindow();
    this.infoModal.show();
  }

  formatDate() {
    var d = new Date(),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

  changegold(event) {

    this.frmpledge.get("gold_charge_percent").setValue(event);
    this.commoncalculation();
  }
  changesilver(event) {
    this.frmpledge.get("silver_charge_percent").setValue(event);
    this.commoncalculation();
  }
}
