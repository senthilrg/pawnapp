import { Component, OnInit } from '@angular/core';

// import { MastersService } from "../../../../providers/masters.service";
import { BsModalService } from "ngx-bootstrap/modal";
import { BsModalRef } from "ngx-bootstrap/modal";
import {AddExpenseCategoryComponent} from '../add-expense-category/add-expense-category.component'
@Component({
  selector: "app-list-expense-category",
  templateUrl: "./list-expense-category.component.html",
  styleUrls: ["./list-expense-category.component.scss"]
})
export class ListExpenseCategoryComponent implements OnInit {
  public expCatList: any[];
  bsModalNewExpCat: BsModalRef;
  constructor(
    // private mastServe: MastersService,
    private modalService: BsModalService
  ) {}

  ngOnInit() {
    this.expCatList = [];
    console.log(" on init ", this.expCatList);
    this.getExpCatList();
  }

  async getExpCatList() {
    // this.expCatList = await this.mastServe.getExpCategoryList();
    console.log(this.expCatList);
  }

  newExpCatPopup() {
    const initialStateProd = {
      title: "New ExpCat",
      class: "modal-md",
      backdrop: true,
      ignoreBackdropClick: true,
      initialState: {}
    };
    this.bsModalNewExpCat = this.modalService.show(
      AddExpenseCategoryComponent,
      initialStateProd
    );

    this.bsModalNewExpCat.content.expCatUpdated.subscribe(data => {
      this.getExpCatList();
      this.bsModalNewExpCat.hide();
    });
  }
}
