import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { BsModalRef } from "ngx-bootstrap/modal";
import { FormControl, FormGroup, Validators } from "@angular/forms";
// import { MastersService } from "../../../../providers/masters.service";
import { ExpensesService } from '../../expenses/shared/expenses.service';


@Component({
  selector: "app-add-expense-category",
  templateUrl: "./add-expense-category.component.html",
  styleUrls: ["./add-expense-category.component.scss"]
})
export class AddExpenseCategoryComponent implements OnInit {
  expCatForm: FormGroup;
  @Output() expCatUpdated = new EventEmitter();

  constructor(
    public bsModalRef: BsModalRef,
    private mastServe: ExpensesService
  ) { }

  ngOnInit() {
    this.expCatForm = new FormGroup({
      expcat_name: new FormControl("", [Validators.required])
    });
  }

  async onExpCatSubmit() {
    console.warn(this.expCatForm.value);
    if (this.expCatForm.valid) {
      const categoryData = new FormData();
      categoryData.append('category_name', this.expCatForm.value.expcat_name);
      this.mastServe.addNewExpCategory(categoryData).subscribe((data: any) => {
        this.expCatUpdated.emit(data);
      },
        error => {
          alert('Error on Submition');
          // this.showLoading = false;
        })
      //  console.log(saveData);

    }

  }
}
