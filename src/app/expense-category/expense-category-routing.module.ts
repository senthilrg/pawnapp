import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListExpenseCategoryComponent} from './list-expense-category/list-expense-category.component'

const routes: Routes = [
  {
    path: "",
    component: ListExpenseCategoryComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExpenseCategoryRoutingModule { }
