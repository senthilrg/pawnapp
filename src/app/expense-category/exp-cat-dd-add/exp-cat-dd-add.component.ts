import { Component, OnInit, Input, ViewChild } from "@angular/core";
// import { MastersService } from "../../../../providers/masters.service";
import { BsModalService } from "ngx-bootstrap/modal";
import { BsModalRef } from "ngx-bootstrap/modal";
import { AddExpenseCategoryComponent } from "../add-expense-category/add-expense-category.component";
import { FormGroup, FormControl } from "@angular/forms";
import { ExpensesService } from '../../expenses/shared/expenses.service';

@Component({
  selector: "app-exp-cat-dd-add",
  templateUrl: "./exp-cat-dd-add.component.html",
  styleUrls: ["./exp-cat-dd-add.component.scss"]
})
export class ExpCatDdAddComponent implements OnInit {
  public expCatList: any[];
  bsModalNewExpCat: BsModalRef;
  @Input() parentForm: FormGroup;
  @Input() inpFormControl: string;
  @Input() showAddnew: boolean;

  constructor(
    private mastServe: ExpensesService,
    private modalService: BsModalService
  ) {
    this.showAddnew = false;
  }

  ngOnInit() {
    this.getExpCatList();
  }

  async getExpCatList() {
     this.mastServe.getExpCategoryList().subscribe(
      (data: any) => {
    
        if (data.length) {
         this.expCatList = data;
        }
        console.log('expense  Category  List', data);
       
      },
      error => {
        window.console.log(error);
        alert("Error on Submition");
      }
    );

  }

  newExpCatPopup() {
    const initialStateProd = {
      title: "New ExpCat",
      class: "modal-md",
      backdrop: true,
      ignoreBackdropClick: true,
      initialState: {}
    };
    this.bsModalNewExpCat = this.modalService.show(
      AddExpenseCategoryComponent,
      initialStateProd
    );

    this.bsModalNewExpCat.content.expCatUpdated.subscribe(data => {
     //  this.expCatList = [...this.expCatList, data];
      this.parentForm.controls[this.inpFormControl].setValue(data);
      this.bsModalNewExpCat.hide();
    });
  }
}
