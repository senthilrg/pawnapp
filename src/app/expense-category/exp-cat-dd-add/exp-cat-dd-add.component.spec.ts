import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpCatDdAddComponent } from './exp-cat-dd-add.component';

describe('ExpCatDdAddComponent', () => {
  let component: ExpCatDdAddComponent;
  let fixture: ComponentFixture<ExpCatDdAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpCatDdAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpCatDdAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
