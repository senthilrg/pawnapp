import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ExpenseCategoryRoutingModule } from './expense-category-routing.module';
import { ListExpenseCategoryComponent } from './list-expense-category/list-expense-category.component';
import { AddExpenseCategoryComponent } from './add-expense-category/add-expense-category.component';
import { ExpCatDdAddComponent } from './exp-cat-dd-add/exp-cat-dd-add.component';
import { AppBootstrapModule } from "../app-bootstarp/app-bootstrap.module";
import {ExpensesService} from '../expenses/shared/expenses.service';

@NgModule({
  declarations: [
    ListExpenseCategoryComponent,
    AddExpenseCategoryComponent,
    ExpCatDdAddComponent
  ],
  imports: [
    CommonModule,
    ExpenseCategoryRoutingModule,
    FormsModule, ReactiveFormsModule,
    AppBootstrapModule
  ],
  entryComponents: [AddExpenseCategoryComponent],
  exports: [ExpCatDdAddComponent],
  providers: [ExpensesService]
})
export class ExpenseCategoryModule {}
