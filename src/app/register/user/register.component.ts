import { Component, OnInit } from '@angular/core';
import { RegisterService } from './register.services'
import {Router,ActivatedRoute} from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-profile',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {
  registerForm: FormGroup;  
  submitted = false;
  selectedFile: File;
  uploadedFile:string = "";
  avatarPath:string = "";
  ShowOTPForm:Boolean = false;
  ShowUsrForm:Boolean = true;
  showError:Boolean = false;
  errorMsg:string = "";
  user_id:string = "";

  constructor(private router:Router,private route:ActivatedRoute,private _registerService:RegisterService,private formBuilder: FormBuilder) {
    this.avatarPath = "../../../assets/img/add-user.png";
    this.showError = false;
    this.errorMsg = "";    
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
        user_id: ['', Validators.required],
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        mobile_no:['', Validators.required],
        email: ['', [Validators.required, Validators.email]],
        pass_word: ['', [Validators.required, Validators.minLength(6)]],
        phone_no:[''],
        address_1_1:[''],
        address_1_2:[''],
        address_1_3:[''],
        address_2_1:[''],
        address_2_2:[''],
        address_2_3:[''],
        address_3_1:[''],
        address_3_2:[''],
        address_3_3:[''],
        area:[''],
        userAvatar:[''],
        pincode:['']
    });
  } 

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
    this.uploadedFile = this.selectedFile.name;

    var reader = new FileReader();
    
    this.registerForm.get('userAvatar').setValue(this.selectedFile);
    reader.onload = (event: any) => {
      //window.console.log(event.target.result);
      //window.console.log(event);
      this.avatarPath = event.target.result;
      
    }
    reader.readAsDataURL(event.target.files[0])
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
      this.submitted = true;
      window.console.log(this.registerForm);
      
      // stop here if form is invalid
      if (this.registerForm.invalid) {
          return;
      }

      const formData = new FormData();
      formData.append("user_name", this.registerForm.get("user_id").value);
      formData.append("user_pass", this.registerForm.get("pass_word").value);      
      formData.append("first_name", this.registerForm.get("firstName").value);      
      formData.append("last_name", this.registerForm.get("lastName").value);      
      formData.append("phone_no", this.registerForm.get("phone_no").value);      
      formData.append("mobile_no", this.registerForm.get("mobile_no").value);      
      formData.append("email", this.registerForm.get("email").value); 
      formData.append("address_1_1", this.registerForm.get("address_1_1").value); 
      formData.append("address_1_2", this.registerForm.get("address_1_2").value); 
      formData.append("address_1_3", this.registerForm.get("address_1_3").value); 
      formData.append("address_2_1", this.registerForm.get("address_2_1").value); 
      formData.append("address_2_2", this.registerForm.get("address_2_2").value);      
      formData.append("address_2_2", this.registerForm.get("address_2_2").value);      
      formData.append("address_2_3", this.registerForm.get("address_2_3").value);      
      formData.append("address_3_1", this.registerForm.get("address_3_1").value);      
      formData.append("address_3_2", this.registerForm.get("address_3_2").value);      
      formData.append("address_3_3", this.registerForm.get("address_3_3").value);    
      formData.append("area", this.registerForm.get("area").value);    
      formData.append("pincode", this.registerForm.get("pincode").value);
      formData.append('userAvatar',this.registerForm.get("userAvatar").value);
      
       this.showError = false;
       this._registerService.AddUser(formData)
        .subscribe(
          (data:any)=>{      
            
           // window.console.log(data);
            alert(data.result);
            if(data.result=="NOK" && data.error==1){
              this.showError = true;
              this.errorMsg = data.msg;
            }else{
              this.showError = false;
              // route to business
              this.router.navigate(['/business/'+data.inserted])
            }
            //this.UserList = data;
          },
          error=>{
            alert("Error on Submition");
            
          }
        );
  }

}
