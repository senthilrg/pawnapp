import { Injectable } from '@angular/core';
import { HttpClient ,HttpHeaders ,HttpParams, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map, filter, catchError, mergeMap } from 'rxjs/operators'
//import {IUserList} from './users';
import { Globals } from '../../../global';
import { RequestOptionsArgs, RequestMethod, RequestOptions } from '@angular/http';
@Injectable()

export class RegisterService{
    private _url: string = Globals.BASE_API_URL;

    constructor(private http: HttpClient){ 
      this._url = this._url + "user/";
    }     

    AddUser(value){
        window.console.log(value);
        let headerOptions = new HttpHeaders();
        //headerOptions.append('Access-Control-Allow-Origin', '*');
        //headerOptions.append('Access-Control-Request-Headers', '*');
        headerOptions.append('Access-Control-Allow-Credentials', 'true');
       // headerOptions.append('Content-Type', 'application/json');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

        const httpParams = new HttpParams().set('values',JSON.stringify(value));
  
        return this.http.post(this._url+"AddUser", value, {
            headers: headerOptions,
        })

    }

    

}