import { Injectable } from '@angular/core';
import { HttpClient ,HttpHeaders ,HttpParams, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map, filter, catchError, mergeMap } from 'rxjs/operators'
//import {IUserList} from './users';
import { Globals } from '../../../global';
import { RequestOptionsArgs, RequestMethod, RequestOptions } from '@angular/http';
@Injectable()

export class BusRegisterService{
    private _url: string = Globals.BASE_API_URL;

    constructor(private http: HttpClient){ 
      this._url = this._url + "business/";
    }     

    AddBusiness(value){
        window.console.log(value);
        let headerOptions = new HttpHeaders();
        headerOptions.append('Access-Control-Allow-Credentials', 'true');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
alert(this._url+"AddBusiness");
        return this.http.post(this._url+"AddBusiness", value, {
            headers: headerOptions,
        });

    }

    

}