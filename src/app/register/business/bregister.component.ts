import { Component, OnInit } from '@angular/core';
import { BusRegisterService } from './bregister.services'
import {Router,ActivatedRoute} from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-profile',
  templateUrl: './bregister.component.html',
  styleUrls: ['./bregister.component.css']
})
export class BusRegisterComponent implements OnInit {
  businessForm: FormGroup;  
  submitted:Boolean = false;
  logoPath:string;
  selectedFile: File;
  uploadedFile:string = "";
  user_id:string = "";
  showError:Boolean = false;

  constructor(private router:Router,private route:ActivatedRoute,private _businessService:BusRegisterService,private formBuilder: FormBuilder) {
    this.logoPath = "../../../assets/img/add-user.png";
    this.showError = false;
    this.route.params.subscribe(params => this.user_id = params.id);
    this.user_id = localStorage.getItem("user_id");
    if(localStorage.getItem("user_id")==""){
      this.router.navigate(['/login/']);
    }
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
    this.uploadedFile = this.selectedFile.name;

    var reader = new FileReader();
    
    this.businessForm.get('businessAvatar').setValue(this.selectedFile);
    reader.onload = (event: any) => {
      //window.console.log(event.target.result);
      //window.console.log(event);
      this.logoPath = event.target.result;      
    }
    reader.readAsDataURL(event.target.files[0])
  }
// convenience getter for easy access to form fields
get f() { return this.businessForm.controls; }

  ngOnInit() {
    this.businessForm = this.formBuilder.group({
      business_name: ['', Validators.required],
      mobile:['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      address_1_1:[''],
      address_1_2:[''],
      address_1_3:[''],
      pan:[''],
      gst:[''],
      cin:[''],
      tan:[''],
      businessAvatar:['']
    });
  }

  onSubmit() {
    this.submitted = true;
    this.showError = false;
    
    window.console.log(this.businessForm);
    alert(this.businessForm.invalid);
    // stop here if form is invalid
    if (this.businessForm.invalid) {
        return;
    }

    const formData = new FormData();
    formData.append("user_id",this.user_id);
    formData.append("business_name", this.businessForm.get("business_name").value);   
    formData.append("mobile", this.businessForm.get("mobile").value);      
    formData.append("email", this.businessForm.get("email").value); 
    formData.append("address_1_1", this.businessForm.get("address_1_1").value); 
    formData.append("address_1_2", this.businessForm.get("address_1_2").value); 
    formData.append("address_1_3", this.businessForm.get("address_1_3").value); 
    formData.append("pan", this.businessForm.get("pan").value); 
    formData.append("gst", this.businessForm.get("gst").value);      
    formData.append("cin", this.businessForm.get("cin").value);      
    formData.append("tan", this.businessForm.get("tan").value);
    formData.append('businessAvatar',this.businessForm.get("businessAvatar").value);
    
     this.showError = false;
     this._businessService.AddBusiness(formData)
      .subscribe(
        (data:any)=>{      
          window.console.log(data);
          this.router.navigate(['/businesslist/']);
        },
        error=>{
          alert("Error on Submition");          
        }
      );
    }

}
