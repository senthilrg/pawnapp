import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusRegisterComponent } from './bregister.component';

describe('BusRegisterComponent', () => {
  let component: BusRegisterComponent;
  let fixture: ComponentFixture<BusRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
