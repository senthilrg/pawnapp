import { Injectable } from '@angular/core';
import { HttpClient ,HttpHeaders ,HttpParams, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map, filter, catchError, mergeMap } from 'rxjs/operators'
import { Globals } from '../../global';
import { RequestOptionsArgs, RequestMethod, RequestOptions } from '@angular/http';
@Injectable()

export class DashboardService{

    constructor(private http: HttpClient){ 
    }     

    InsertStartValue(value){
        //window.console.log(value);
        let headerOptions = new HttpHeaders();
        headerOptions.append('Access-Control-Allow-Credentials', 'true');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

        return this.http.post(Globals.CI_BASE_URL+"TodaySummary/addStartValue", value, {
            headers: headerOptions,
        });
    }

    GetTodaySummary(value){
        //window.console.log(value);
        let headerOptions = new HttpHeaders();
        headerOptions.append('Access-Control-Allow-Credentials', 'true');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

        return this.http.get(Globals.CI_BASE_URL+"TodaySummary/GetTodaySummary/?tdate="+value,  {
            headers: headerOptions,
        });
    }
    GetRegister(bus_id) {
        let headerOptions = new HttpHeaders();
        headerOptions.append('Access-Control-Allow-Credentials', 'true');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
       
        return this.http.get(Globals.CI_BASE_URL+"RegisterList/?bus_id="+bus_id + '&rand='+ Math.random(),  {
            headers: headerOptions,
        });
    }
    GetBusSettings(bus_id: string) {
        let headerOptions = new HttpHeaders();
        headerOptions.append('Access-Control-Allow-Credentials', 'true');
        headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        return this.http.get(Globals.CI_BASE_URL + "GetBusinessSettings/?bus_id=" + bus_id+'&rand='+ Math.random(), { headers: headerOptions });
    }


}