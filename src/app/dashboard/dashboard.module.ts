import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { HttpClient,HttpClientModule } from '@angular/common/http';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { DashboardService } from './dashboard.services';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers:[DashboardService],
  declarations: []
})
export class DashboardModule { }