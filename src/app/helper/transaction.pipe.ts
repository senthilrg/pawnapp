import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'transaction'
})
export class TransactionPipe implements PipeTransform {

  transform(value: any, args?: any): any {
   // full_amount : 1; interest : 2; partial : 3;
   const typevalue = Number(value);
    if (typevalue === 1) {
      return 'Full Amount';
    }else if (typevalue === 2) {
      return 'Interest';
    }else if (typevalue === 3) {
      return 'Partial';
    }else {
      return null;
    }
  }

}
