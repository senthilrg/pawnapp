import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pledge'
})
export class PledgePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const typevalue = Number(value);
    if (typevalue === 1) {
      return 'Active';
    }else if (typevalue === 2) {
      return 'In Active';
    }else {
      return null;
    }
  }

}
