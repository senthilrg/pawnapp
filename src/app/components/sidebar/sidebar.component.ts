import { Component, OnInit } from '@angular/core';
import { Globals } from '../../../global';
import { DashboardService } from '../../dashboard/dashboard.services';
import { Router, ActivatedRoute } from '@angular/router';

declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  { path: '/dashboard', title: 'Dashboard', icon: 'fa-desktop', class: '' },
  { path: '/pledge', title: 'Pledge', icon: 'fa-diamond', class: '' },

  { path: '/register', title: 'User Register', icon: 'fa-user-o', class: '' },
  { path: '/business', title: 'Business', icon: 'fa-briefcase', class: '' },
  { path: '/', title: 'Manage Business', icon: 'fa-briefcase', class: '' },
  { path: '/settings', title: 'Settings', icon: 'fa-cog', class: '' },
  { path: '/reports', title: 'Reports', icon: 'fa-file-text-o', class: '' },
  { path: '/expense', title: 'Expense', icon: 'fa-money', class: '' },

  { path: '/redeem', title: 'Redumption', icon: 'fa-minus', class: '' },
  { path: '/redumption', title: 'Re-Pledge', icon: 'fa-repeat', class: '' },
  /*{ path: '/icons', title: 'Icons',  icon:'education_atom', class: '' },
  { path: '/maps', title: 'Maps',  icon:'location_map-big', class: '' },
  { path: '/notifications', title: 'Notifications',  icon:'ui-1_bell-53', class: '' },

  { path: '/user-profile', title: 'User Profile',  icon:'users_single-02', class: '' },
  { path: '/table-list', title: 'Table List',  icon:'design_bullet-list-67', class: '' },
  { path: '/typography', title: 'Typography',  icon:'text_caps-small', class: '' },
  { path: '/upgrade', title: 'Upgrade to PRO',  icon:'objects_spaceship', class: 'active active-pro' }*/

];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  businessName: string = "";
  bus_photo: string = "";
  private close_status = false;
  private register_status = false;
  constructor(private _dashboardService: DashboardService, private router: Router) { }

  ngOnInit() {
    console.log('side bar')
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.businessName = localStorage.getItem("bus_name");
    this.bus_photo = Globals.BUS_IMAGE + localStorage.getItem("bus_photo");

    this._dashboardService.GetRegister(localStorage.getItem("bus_id")).subscribe(
      (data: any) => {
        if (data.length) {

          const date1: any = new Date(data[0].created_date);

          const date2: any = new Date();
          const diffTime = Math.abs(date2 - date1);
          const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
          const cash_det = Number(data[0].cash_in) - Number(data[0].cash_out);
          if (diffDays > 1) {
            this.close_status = true;
          } 
        }else {
          console.log('please Add Register');
          this.register_status = true;
        }

      },
      error => {
        window.console.log(error);
        alert("Error on Submition");
      }
    );
  }
  isMobileMenu() {
    if (window.innerWidth > 991) {
      return false;
    }
    return true;
  };

  navigation(path) {
    // [routerLink]="[menuItem.path]"
    console.log('path', path);
    console.log('close_status', this.close_status);
    if (this.close_status) {
      alert('Please close the register');
      return false;
    }else if (this.register_status) {
      alert('Please open the register');
      return false;
    }
    this.router.navigate([path]);

  }
}
