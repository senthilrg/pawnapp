import { Component, OnInit, ViewChild } from '@angular/core';
import { PledgelistService } from '../pledge/list/pledgelist.services';
import { EditPledgeService } from '../pledge/manage/editpledge.services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Globals } from '../../global';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-redeem',
  templateUrl: './redeem.component.html',
  styleUrls: ['./redeem.component.scss']
})
export class RedeemComponent implements OnInit {
  showLoading: boolean = true;
  public referenceLoading = true;
  public pledgeListLoading = false;
  public redeemLoading = false;
  // AB-0000-85963
  bill: string = "";
  PledgeList = [];
  customerPath: string;
  frmpledge: FormGroup;
  MainPledgeItems = [];
  PledgeImgPath: string = "";
  ItemList = [];
  gold_rate: number = 0;
  silver_rate: number = 0;
  custFieldEnabled: Boolean = true;
  tot_pcs: number = 0;
  tot_wgt: number = 0;
  tot_wst: number = 0;
  tot_net_wgt: number = 0;
  selectedPledgeItm: any = [];
  PledgeImgName: string = "";
  selectedFile: File;
  uploadedFile: string = "";
  dropdownSettings = {};
  @ViewChild('infoModal') infoModal: ModalDirective;
  @ViewChild('paymentModal') paymentModal: ModalDirective;
  public paymentView = false;
  public paidAmount: number = 0;
  public partialAmount: number = 0;
  public paymentType: number = 0;
  public interestAmt: number = 0;
  public gold_rate_interest = [];
  public silver_rate_interest = [];

  constructor(private _pledgeService: PledgelistService, private _pledgeeditService: EditPledgeService, private formBuilder: FormBuilder,
    private router: Router) { }

  ngOnInit() {
    this.showLoading = false;
    this.frmpledge = this.formBuilder.group({
      bill_no_1: ['', Validators.required],
      bill_no_2: ['', Validators.required],
      date_of_bill: ['', Validators.required],
      cust_name: ['', Validators.required],
      father_name: ['', Validators.required],
      phone_no: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      area: ['', Validators.required],
      pin_code: ['', Validators.required],
      address_1: [''],
      address_2: [''],
      address_3: [''],
      price: [''],
      roi: [''],
      total_month: [''],
      gold_amt_principal: [''],
      silver_amt_principal: [''],
      transaction_type: [''],
      service_charge: [''],
      interest: [''],
      amount: ['', Validators.required],
      customerAvatar: [''],
      pledge_id: [0],
      pledge_status: [1],
      pledgeItm: [''],
      pledgeItemAvatar: [''],
      find_user_input: [''],
      customer_id: [0],
      item_name: [''],
      no_of_pcs: [''],
      wgt_gms: [''],
      waste_gms: [''],
      net_wgt: [''],
      selectedPledgeItem: [''],
      selectedPledgeName: [''],
      selectedPledgeType: [''],
      gold_price: [''],
      gold_rate_interest: [''],
      gold_interest: [''],
      gold_service_charge: [''],
      gold_charge_percent: [''],
      gold_amount: [''],
      silver_amount: [''],
      silver_price: [''],
      silver_rate_interest: [''],
      silver_interest: [''],
      silver_service_charge: [''],
      silver_charge_percent: ['']
    });


    this.dropdownSettings = {
      singleSelection: true,
      text: "Select",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: false,
      classes: "myclass custom-class"
    };
    this.frmpledge.get("date_of_bill").setValue(this.formatDate());
  }
  get f() { return this.frmpledge.controls; }

  SearchPledge() {
    this.showLoading = true;
    // localStorage.getItem("bus_id")

    this._pledgeService.GetPledges(localStorage.getItem("bus_id"), this.bill)
      .subscribe(
        (data: any) => {

          if (data.length > 0) {

            this.PledgeList = data;
            this.showLoading = false;
            this.redeemLoading = false;
            this.pledgeListLoading = true;
            this.referenceLoading = false;
          }
        },
        error => {
          this.showLoading = false;
          window.console.log(error);
          alert("Error on Submition");
        }
      );
  }



  managePledge(bill_no: string) {
    this.redeemLoading = true;
    this.pledgeListLoading = false;
    this.referenceLoading = false;
    this.GetItemMaster();
    this.getPledge(bill_no);

  }
  getPledge(id) {
    this._pledgeeditService.GetPledgeData(id).subscribe(
      (data: any) => {
        if (data.length) {
          console.log('pledge data ', data)
          const pledgeVal = data[0];

          this.frmpledge.get("pledge_id").setValue(pledgeVal.pledge_id);
          this.pledgeItem(pledgeVal.pledge_id);
          this.Transaction(pledgeVal.pledge_id);
          if (pledgeVal.pledge_bill_no != undefined) {
            var billarray = pledgeVal.pledge_bill_no.split("-");

            this.frmpledge.get("bill_no_1").setValue(billarray[0]);
            this.frmpledge.get("bill_no_2").setValue(billarray[1]);
          }

          this.frmpledge.get("cust_name").setValue(pledgeVal.customer_name);
          this.frmpledge.get("father_name").setValue(pledgeVal.customer_father);
          this.frmpledge.get("phone_no").setValue(pledgeVal.customer_mobile);
          this.frmpledge.get("email").setValue(pledgeVal.customer_email);
          this.frmpledge.get("pin_code").setValue(pledgeVal.pincode);
          this.frmpledge.get("area").setValue(pledgeVal.area);
          this.frmpledge.get("customer_id").setValue(pledgeVal.customer_id);


          if (pledgeVal.customer_address) {
            var res = pledgeVal.customer_address.split(",");
            const add1 = res[0] ? res[0] : '';
            const add2 = res[1] ? res[1] : '';
            const add3 = res[2] ? res[2] : '';
            this.frmpledge.get("address_1").setValue(add1);
            this.frmpledge.get("address_2").setValue(add2);
            this.frmpledge.get("address_3").setValue(add3);
          }

          this.gold_rate = Number(pledgeVal.gold_pledge_price);
          this.frmpledge.get("gold_amount").setValue(Number(pledgeVal.gold_pledge_price));
          this.frmpledge.get("gold_price").setValue(Number(pledgeVal.gold_pledge_price));
          this.frmpledge.get("gold_interest").setValue(Number(pledgeVal.gold_interest));
          this.gold_rate_interest = JSON.parse(pledgeVal.gold_roi);
          this.frmpledge.get("gold_rate_interest").setValue(JSON.parse(pledgeVal.gold_roi));
          this.frmpledge.get("gold_charge_percent").setValue(Number(pledgeVal.gold_service_charge));

          this.silver_rate = Number(pledgeVal.silver_pledge_price);
          this.frmpledge.get("silver_amount").setValue(Number(pledgeVal.silver_pledge_price));
          this.frmpledge.get("silver_price").setValue(Number(pledgeVal.silver_pledge_price));
          this.frmpledge.get("silver_interest").setValue(Number(pledgeVal.silver_interest));
          this.silver_rate_interest = JSON.parse(pledgeVal.silver_roi);
          this.frmpledge.get("silver_rate_interest").setValue(JSON.parse(pledgeVal.silver_roi));
          this.frmpledge.get("silver_charge_percent").setValue(Number(pledgeVal.silver_service_charge));
          this.frmpledge.get("service_charge").setValue(Number(pledgeVal.service_charge));



          this.customerPath = Globals.CUSTOMER_IMAGE + pledgeVal.photo;

        }
      });
  }

  Transaction(id) {
    this._pledgeeditService.GetTransaction(id).subscribe(async (data: any) => {
      console.log('transactions', data);
      if (data.length) {
        const last_trans = data.slice(-1)[0];
        console.log('last_trans', last_trans);
        if (last_trans != undefined) {
          this.frmpledge.get("gold_amt_principal").setValue(Number(last_trans.gold_amt_principal));
          this.frmpledge.get("silver_amt_principal").setValue(Number(last_trans.silver_amt_principal));
          this.frmpledge.get("transaction_type").setValue(Number(last_trans.transaction_type));
          
          const current_date: any = new Date();
          const trans_date: any = new Date(last_trans.transaction_date);
          // console.log('trans_date', trans_date);
          // console.log('current_date', current_date);
          const diffTime = Math.abs(current_date - trans_date);
          const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
          console.log(diffDays, '???');
          const div = Math.trunc(diffDays / 30);
          const rem = diffDays % 30;
          // console.log('div : ', div , 'rem :', rem);
          const rem_div = (div > 0) ? div : 0
          const rem_bal = (rem_div > 0) ? ((rem > 0) ? 1 : 0) : 0;
          const tot_month = rem_div + rem_bal;
          console.log('tot_month', tot_month);
          this.frmpledge.get("total_month").setValue(Number(tot_month));
          this.reCalcPledgeItems();
          
        }
      }
    });
  }
  pledgeItem(id) {
    this.MainPledgeItems = [];
    this._pledgeeditService.GetPledgeItem(id).subscribe(async (data: any) => {

      if (data.length) {
        const itemData = this.ItemList.filter(role => data.find(item => item.pledge_item == role.id));
        for (const item of data) {

          const pledgeitem = Globals.PLEDGE_IMAGE + item.pledge_image;
          console.log('pledgeitem', item)
          const typeItem = itemData.find(itemtype => itemtype.id == item.pledge_item);
          let itm = {
            "imgPath": pledgeitem,
            "itemType": (typeItem) ? typeItem.itemType : null,
            "itemName": (typeItem) ? typeItem.itemName : null,
            "imgFile": item.pledge_image,
            "pledgeItm": item.pledge_item,
            "no_of_pcs": item.no_of_pcs,
            "wgt_gms": item.no_of_wgts,
            "waste_gms": item.wgt_grms,
            "net_wgt": item.net_wgts,
            "pledge_item_id": item.pledge_item_id
          }
          if (item.is_deleted == '0') {
            this.MainPledgeItems.push(itm);
          }
         
        }
        const conditionBase = await this.MainPledgeItems;

      }

    });

  }

  GetItemMaster() {
    this._pledgeeditService.GetItems()
      .subscribe(
        (data: any) => {
          if (data.length > 0) {
            data.forEach(element => {
              let obj = { "id": element.item_id, "itemName": element.item_type + "-" + element.item_name, "itemType": element.item_type };
              this.ItemList.push(obj);
            });
          }
        },
        error => {
          window.console.log(error);
          alert("Error on Submition");
        }
      );
  }


  removePledgeItm(index) {
    this.MainPledgeItems.splice(index, 1);
    this.reCalcPledgeItems();
  }

  reCalcPledgeItems() {
    this.tot_pcs = 0;
    this.tot_net_wgt = 0;
    this.tot_wgt = 0;
    this.tot_wst = 0;
    const silverObj = {
      sil_pcs: 0,
      sil_wgt: 0,
      sil_wst: 0,
      sil_net_wgt: 0
    };
    const goldObj = {
      gol_pcs: 0,
      gol_wgt: 0,
      gol_wst: 0,
      gol_net_wgt: 0
    };

    if (this.MainPledgeItems.length > 0) {

      this.MainPledgeItems.forEach(element => {
        if (element.itemType == 'silver') {

          silverObj.sil_net_wgt = silverObj.sil_net_wgt + parseInt(element.net_wgt);
          silverObj.sil_pcs = silverObj.sil_pcs + parseInt(element.no_of_pcs);
          silverObj.sil_wgt = silverObj.sil_wgt + parseInt(element.wgt_gms);
          silverObj.sil_wst = silverObj.sil_wst + parseInt(element.waste_gms);
        } else if (element.itemType == 'gold') {
          goldObj.gol_net_wgt = goldObj.gol_net_wgt + parseInt(element.net_wgt);
          goldObj.gol_pcs = goldObj.gol_pcs + parseInt(element.no_of_pcs);
          goldObj.gol_wgt = goldObj.gol_wgt + parseInt(element.wgt_gms);
          goldObj.gol_wst = goldObj.gol_wst + parseInt(element.waste_gms);
        }
        this.tot_pcs = this.tot_pcs + parseInt(element.no_of_pcs);
        this.tot_wgt = this.tot_wgt + parseInt(element.wgt_gms);
        this.tot_wst = this.tot_wst + parseInt(element.waste_gms);
        this.tot_net_wgt = this.tot_net_wgt + parseInt(element.net_wgt);
      });


      this.frmpledge.get("gold_price").setValue(goldObj.gol_net_wgt * this.gold_rate);
      this.frmpledge.get("gold_charge_percent").setValue(0);
      if (this.frmpledge.get("gold_price").value !== 0) {
        const goldPrice = goldObj.gol_net_wgt * this.gold_rate;

        for (const charge of this.gold_rate_interest) {

          if ((goldPrice >= charge.chargeFromGold) && (charge.chargeToGold >= goldPrice)) {

            this.frmpledge.get("gold_charge_percent").setValue(charge.chargePercentGold);

          }
        }

      }

      this.frmpledge.get("silver_price").setValue(silverObj.sil_net_wgt * this.silver_rate);
      this.frmpledge.get("silver_charge_percent").setValue(0);
      if (this.frmpledge.get("silver_price").value !== 0) {
        const silverPrice = silverObj.sil_net_wgt * this.silver_rate;

        for (const charge of this.silver_rate_interest) {

          if ((silverPrice >= charge.chargeFromSilver) && (charge.chargeToSilver >= silverPrice)) {

            this.frmpledge.get("silver_charge_percent").setValue(charge.chargePercentSilver);

          }
        }

      }

      this.commoncalculation();


    }
  }
  commoncalculation() {

    if (this.frmpledge.get("gold_price").value !== 0) {
      const gold_charge = this.frmpledge.get("gold_charge_percent").value;
      const interestg = (Number(gold_charge) / 100) * this.frmpledge.get("gold_price").value;

      this.frmpledge.get("gold_interest").setValue(interestg);
    } else {
      this.frmpledge.get("gold_interest").setValue(0);
    }

    if (this.frmpledge.get("silver_price").value !== 0) {
      const silver_charge = this.frmpledge.get("silver_charge_percent").value;
      var interest = (Number(silver_charge) / 100) * this.frmpledge.get("silver_price").value;
      this.frmpledge.get("silver_interest").setValue(interest);
    } else {
      this.frmpledge.get("silver_interest").setValue(0);
    }


    const ser_chr = (this.frmpledge.get("service_charge").value) ? this.frmpledge.get("service_charge").value : 0;
    console.log('ser_chr', ser_chr);

    const tot_price = this.frmpledge.get("gold_price").value + this.frmpledge.get("silver_price").value;
    console.log('tot_price', tot_price);

    const intrest = ((this.frmpledge.get("gold_interest").value + this.frmpledge.get("silver_interest").value ) * 
    this.frmpledge.get("total_month").value );
    console.log('intrest', intrest);
    const redeem_amt = intrest + (ser_chr * this.frmpledge.get("total_month").value);
    console.log('redeem_amt', redeem_amt);
    this.paidAmount = tot_price;
    this.interestAmt = redeem_amt;
    this.partialAmount = redeem_amt;
    this.frmpledge.get("amount").setValue(tot_price + redeem_amt);

  }

  editPledgeItm(pledgeItms) {
    this.ResetPledgeWindow();
    this.PledgeImgPath = Globals.PLEDGE_IMAGE + pledgeItms.imgFile;
    this.frmpledge.get('pledgeItm').setValue(pledgeItms.pledgeItm);
    this.frmpledge.get('selectedPledgeItem').setValue(pledgeItms.itemName);
    this.frmpledge.get('no_of_pcs').setValue(pledgeItms.no_of_pcs);
    this.frmpledge.get('wgt_gms').setValue(pledgeItms.wgt_gms);
    this.frmpledge.get('waste_gms').setValue(pledgeItms.waste_gms);
    this.frmpledge.get('net_wgt').setValue(pledgeItms.net_wgt);

    this.infoModal.show();
  }
  partialamount() {
    this.paymentView = true;
    this.paymentType = this.frmpledge.get('transaction_type').value;
    this.paymentModal.show();
  }

  fullamount() {
    this.paymentView = false;
    this.paidAmount = this.frmpledge.get('amount').value;
    this.paymentType = 1;
    this.paymentModal.show();

  }

  payment() {
    // console.log('partialAmount', this.partialAmount);
    // this.showLoading = true;
    //window.console.log(this.frmpledge);
    const amt_rec = (this.paymentView) ? String(this.partialAmount) : String(this.paidAmount);
    const gold_amt_princ = (this.paymentView) ? this.frmpledge.get("gold_amt_principal").value : 0;
    const silver_amt_princ = (this.paymentView) ? this.frmpledge.get("silver_amt_principal").value : 0;
    const formData = new FormData();
    formData.append("pledge_id", this.frmpledge.get("pledge_id").value);
    formData.append("transaction_type", String(this.paymentType)); // full_amount : 1; interest : 2; partial : 3;
    formData.append('amount_received', amt_rec);
    formData.append('gold_price', gold_amt_princ);
    formData.append('silver_price', silver_amt_princ);
    formData.append('bus_id', localStorage.getItem("bus_id"));
    
    this._pledgeeditService.updatetransactions(formData).subscribe(
      (data: any) => {
        console.log('data', data)
       // this.ResetPledgeWindow();

        this.showLoading = false;
        this.paymentModal.hide();
        this.router.navigate(['/dashboard/']);
        // window.console.log(data);
        //alert(data);
        /*if(data.result=="NOK" && data.error==1){
          
        }else{
          
        }*/
        //this.UserList = data;
      },
      error => {
        alert("Error on Submition");
        this.showLoading = false;
      }
    );
  }

  partialcalculation(event) {
    console.log('partialAmount', this.partialAmount);

    if (this.frmpledge.get("gold_price").value !== 0) {
      const roig = this.frmpledge.get("gold_rate_interest").value;
      const service_chargeg = this.frmpledge.get("gold_charge_percent").value;
      const interestg = (this.frmpledge.get("gold_interest").value / 100) * this.frmpledge.get("gold_price").value;
      this.frmpledge.get("gold_amount").setValue(this.frmpledge.get("gold_price").value - roig - service_chargeg - interestg);
    } else {
      this.frmpledge.get("gold_amount").setValue(0);
    }


    if (this.frmpledge.get("silver_price").value !== 0) {
      const roi = this.frmpledge.get("silver_rate_interest").value;
      const service_charge = this.frmpledge.get("silver_charge_percent").value;
      var interest = (this.frmpledge.get("silver_interest").value / 100) * this.frmpledge.get("silver_price").value;
      this.frmpledge.get("silver_amount").setValue(this.frmpledge.get("silver_price").value - roi - service_charge - interest);
    } else {
      this.frmpledge.get("silver_amount").setValue(0);
    }



    const total_amt = this.frmpledge.get("gold_amount").value + this.frmpledge.get("silver_amount").value;
    console.log('total_amt', total_amt);

  }



  ResetPledgeWindow() {
    this.PledgeImgPath = "../../assets/img/add-prod.jpg";
    this.frmpledge.get('pledgeItemAvatar').setValue("");
    this.frmpledge.get('selectedPledgeItem').setValue("");
    this.frmpledge.get('no_of_pcs').setValue("");
    this.frmpledge.get('wgt_gms').setValue("");
    this.frmpledge.get('waste_gms').setValue("");
    this.frmpledge.get('net_wgt').setValue("");
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
    this.uploadedFile = this.selectedFile.name;

    var reader = new FileReader();

    this.frmpledge.get('customerAvatar').setValue(this.selectedFile);
    reader.onload = (event: any) => {
      this.customerPath = event.target.result;
    }
    reader.readAsDataURL(event.target.files[0])
  }

  onPledgeChanged(event) {
    this.frmpledge.get('pledgeItemAvatar').setValue(event.target.files[0]);
    this.PledgeImgName = event.target.files[0].name;
    var reader = new FileReader();

    reader.onload = (event: any) => {
      this.PledgeImgPath = event.target.result;
    }
    reader.readAsDataURL(event.target.files[0])
  }

  onPledgeItemSelect(item: any) {
    const res = item.itemName.split("-");
    this.selectedPledgeItm = item.pledge_item;
    this.frmpledge.get('selectedPledgeName').setValue(res[0]);
  }
  OnPledgeItemDeSelect(item: any) {
    this.selectedPledgeItm = [];
  }
  formatDate() {
    var d = new Date(),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

  AddPledgeItem() {
    const pledge_id = this.frmpledge.get('pledgeItm').value;
    let targetProductline = this.MainPledgeItems.find(x => x.pledgeItm == pledge_id);
    if (targetProductline) {
      const noPcs = this.frmpledge.get('no_of_pcs').value;
      targetProductline.no_of_pcs = noPcs
    }
    this.infoModal.hide();

    this.reCalcPledgeItems();
  }
}
