import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from '../login/login.services';

@Injectable({
  providedIn: 'root'
})
export class BusinessGuard implements CanActivate {
  constructor(
    private router: Router, private authService: LoginService
  ) { }
  canActivate(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean | UrlTree {


    const that = this;
    if (!this.authService.isBusinessIn) {
      alert('You are not allowed to view this page');

      this.router.navigate(['/'], { queryParams: { returnUrl: state.url } });
      return false;
    } else {
      // return true;

      this.authService.isBusinessSettingIn().then(result => {
        console.log('condition :-', result)
        if (!result) {
          this.router.navigate(['/settings'], { queryParams: { returnUrl: state.url } });
          return false;

        } else {
          that.authService.isRegisterIn().then(final => {
            console.log('final', final);
            if (!final.condition) {

              alert(final.status);
              

              var n = this.router.url.search("dashboard");
              console.log('n', n)
              if (n == -1) {
                this.router.navigate(['/dashboard']);
              }
              
              
              return false;
            } else {
              console.log('returnUrl', this.router.url);
              console.log('route',state.url);

              var n = this.router.url.search("pledge");
              console.log('n', n);
              alert('change')
              if (n == -1) {
                this.router.navigate(['/pledge']);
              }
              // this.router.navigate([state.url], { queryParams: { returnUrl: state.url } });
              return true;
            }
          })


        }
      })


    }


    // return true;
  }

}




