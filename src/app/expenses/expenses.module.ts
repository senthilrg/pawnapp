import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExpensesRoutingModule } from './expenses-routing.module';
import { ListExpensesComponent } from './list-expenses/list-expenses.component';
import { AddExpensesComponent } from './add-expenses/add-expenses.component';

import {ExpenseCategoryModule} from '../expense-category/expense-category.module';
import { AppBootstrapModule } from "../app-bootstarp/app-bootstrap.module";
import {ExpensesService} from '../expenses/shared/expenses.service';



@NgModule({
  declarations: [ListExpensesComponent, AddExpensesComponent],
  imports: [
    CommonModule,
    ExpensesRoutingModule,
    ExpenseCategoryModule,
    FormsModule, ReactiveFormsModule,
    AppBootstrapModule
  ],
  entryComponents: [AddExpensesComponent],
  providers: [ExpensesService]
})
export class ExpensesModule {}
