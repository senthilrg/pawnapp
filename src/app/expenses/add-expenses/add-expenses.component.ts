import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { BsModalRef } from "ngx-bootstrap/modal";
import { FormControl, FormGroup } from "@angular/forms";
import {ExpensesService} from '../shared/expenses.service';


@Component({
  // tslint:disable-next-line:component-selector
  selector: "app-add-expenses",
  templateUrl: "./add-expenses.component.html",
  styleUrls: ["./add-expenses.component.scss"]
})
export class AddExpensesComponent implements OnInit {
  showLoading: boolean = true;
  expenseForm: FormGroup;
  @Output() expenseUpdated = new EventEmitter();
  constructor(
    public bsModalRef: BsModalRef,
     private accountServe: ExpensesService
  ) {}

  ngOnInit() {
    this.expenseForm = new FormGroup({
      exp_title: new FormControl(),
      exp_date: new FormControl(),
      exp_amount: new FormControl(),
      exp_category: new FormControl()
    });
    this.expenseForm.get("exp_date").setValue(this.formatDate());
  }

  async onExpenseSubmit() {
    console.log('form' , this.expenseForm.value);
  
    const categoryData = new FormData();
      categoryData.append('bus_id', localStorage.getItem('bus_id'));
      categoryData.append('expense_category', this.expenseForm.value.exp_category);
      categoryData.append('expense_amount', this.expenseForm.value.exp_amount);
      categoryData.append('expense_note', this.expenseForm.value.exp_title);
      categoryData.append('expense_date', this.expenseForm.value.exp_date);
      categoryData.append('user_name', localStorage.getItem('user_name'));
    

     this.accountServe.addNewExpenses(categoryData).subscribe(
      (data: any) => {
    
        if (data.length) {
        // this.expCatList = data;
        }
        this.expenseUpdated.emit(data);
       
      },
      error => {
        window.console.log(error);
        alert("Error on Submition");
      }
    );
    // 
  }
  formatDate() {
    var d = new Date(),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }
}
