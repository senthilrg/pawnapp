import { Injectable } from '@angular/core';
import { Globals } from '../../../global';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ExpensesService {

  constructor(private http: HttpClient) { }

  getExpensesList(bus_id) {
    let headerOptions = new HttpHeaders();
    headerOptions.append('Access-Control-Allow-Credentials', 'true');
    headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
    headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

    return this.http.get(Globals.CI_BASE_URL + "listallExpenses/?bus_id=" + bus_id + '&rand=' + Math.random(), {
      headers: headerOptions,
    });
  }
  addNewExpCategory(value) {

    let headerOptions = new HttpHeaders();
    headerOptions.append('Access-Control-Allow-Credentials', 'true');
    headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
    headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    return this.http.post(Globals.CI_BASE_URL + "AddCategory", value, {
      headers: headerOptions,
    });
  }
  getExpCategoryList() {
    let headerOptions = new HttpHeaders();
    headerOptions.append('Access-Control-Allow-Credentials', 'true');
    headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
    headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

    return this.http.get(Globals.CI_BASE_URL + "listallExpensesCategory", {
      headers: headerOptions,
    });
  }

  addNewExpenses(value) {
    let headerOptions = new HttpHeaders();
    headerOptions.append('Access-Control-Allow-Credentials', 'true');
    headerOptions.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
    headerOptions.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    // 'http://localhost/pawn_ci_api/'
    return this.http.post(Globals.CI_BASE_URL + "AddExpenses", value, {
      headers: headerOptions,
    });
  }
}
