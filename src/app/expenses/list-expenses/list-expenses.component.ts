import { Component, OnInit } from '@angular/core';

// import { AccountsService } from '../../../../providers/accounts.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal';
import {AddExpensesComponent} from '../add-expenses/add-expenses.component';
import {ExpensesService} from '../shared/expenses.service';

@Component({
  selector: "app-list-expenses",
  templateUrl: "./list-expenses.component.html",
  styleUrls: ["./list-expenses.component.scss"]
})
export class ListExpensesComponent implements OnInit {
  showLoading: boolean = true;
  public expenseList: any[];
  bsModalNewExpense: BsModalRef;
  constructor( private accServe: ExpensesService,
    private modalService: BsModalService
  ) {
    this.showLoading = true;
  }

  ngOnInit() {
    this.expenseList = [];

    this.getExpenseList();
    this.showLoading = false;
  }

  async getExpenseList() {

     this.accServe.getExpensesList(localStorage.getItem("bus_id")).subscribe(
      (data: any) => {
        if (data.length) {
          this.expenseList = data;
        }
        // console.log('expense List', data);
       
      },
      error => {
        window.console.log(error);
        alert("Error on Submition");
      }
    );
   // console.log(this.expenseList);
  }

  newExpensePopup() {
    const initialStateProd = {
      title: "Add Expense",
      class: "modal-md",
      backdrop: true,
      ignoreBackdropClick: true,
      initialState: {}
    };
    this.bsModalNewExpense = this.modalService.show(
      AddExpensesComponent,
      initialStateProd
    );

    this.bsModalNewExpense.content.expenseUpdated.subscribe(data => {
      this.getExpenseList();
      this.bsModalNewExpense.hide();
    });
  }
}
